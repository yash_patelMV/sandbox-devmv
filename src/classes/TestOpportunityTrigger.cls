@isTest
private class TestOpportunityTrigger {

	private static testMethod void test() {
	     
	    Account acc = new Account(name='test');
        insert acc;
        
        Opportunity opp = new Opportunity(Name = 'test', StageName = 'Prospecting', CloseDate = System.today(), AccountId = acc.Id); 
        
        Test.startTest();
        insert opp;
        update opp;
        delete opp;
        undelete opp;
        Test.stopTest();
        
	}

}