public class Case_FormController {
    
    public Case     CaseObj{get; set;}
    public String   FirstName{get; set;}
    public String   LastName{get; set;}
    public String   AccountName{get; set;}
    public String   AccountNumber{get; set;}
    public String   EmailId{get;set;}
    public String   PhoneNumber{get;set;}
    public String   PrefferdMehtod{get;set;}
    public String   InqueryReason{get;set;}
    public String   Urgency{get;set;}
    public String   Description{get;set;}
    public Boolean  isChecked{get;set;}
    public String   Submittedby{get; set;}
    
    public List<SelectOption> prefferedMethodList {get;set;}
    public List<SelectOption> InqueryReasonsList {get;set;}
    public List<SelectOption> UrgencyTypeList {get;set;}
    
    public Case_FormController(ApexPages.StandardController controller){
        CaseObj = new Case();
        getPrefferedMethodList();
        getInqueryList();
        getUrgencyList();
    }
    
    //Method for save case record from webform
    public void saveRecord(){
        
        Decimal flag = 0;
        
        /* Data validation from client's input in webform */
        String ErrorMsg = 'Please input correct data or fill out all required fields: ';
        
        if(String.isBlank(FirstName) || String.isEmpty(FirstName)){
            flag = 1;
            ErrorMsg += 'First Name, ';
        }
        
        if(String.isBlank(LastName) || String.isEmpty(LastName)){
            flag = 1;
            ErrorMsg += flag == 0 ? 'Last Name, ' : ' Last Name,';
        }
        
        if(String.isBlank(AccountName) || String.isEmpty(AccountName)){
            flag = 1;
            ErrorMsg += flag == 0 ? 'Account Name, ' : ' Account Name,';
        }

        if(String.isBlank(InqueryReason) || String.isEmpty(InqueryReason)){
            flag = 1;
            ErrorMsg += flag == 0 ? 'Inquiry Reason, ' : ' Inquiry Reason,';
        }
        
        if(String.isBlank(Submittedby) || String.isEmpty(Submittedby)){
            flag = 1;
            ErrorMsg += flag == 0 ? 'Your name or Agent Id , ' : ' Your name or Agent Id,';
        }

        String Email = EmailId.deleteWhitespace();
        System.debug('Email Address:: '+Email);
        if(!Pattern.matches('^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,63}$', Email) && String.isNotBlank(EmailId) && String.isNotEmpty(EmailId)){
            flag = 1;
            ErrorMsg += flag == 0 ? 'Email Format is Wrong, ' : ' Email Format is Wrong,';
        }
        
        if(flag == 1){
            
            if(ErrorMsg.SubString(ErrorMsg.length()-1) == ','){
                ErrorMsg = ErrorMsg.removeEnd(',');
            }
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,ErrorMsg));

        }else{
            
             try{
                
                String oppId = '';
                String AccId = '';

                List<Opportunity> oppList = new List<Opportunity>();
                 
                oppList = [SELECT Id,Name,Account__c,AccountId FROM Opportunity WHERE Account__c =: AccountNumber AND Type != 'Cancelled Business' AND Type != 'Duplicate Account' ORDER BY CreatedDate DESC Limit 1];
                if(oppList.size()>0){
                    oppId = oppList[0].Id;
                    AccId = oppList[0].AccountId;
                }

                // Case Creation
                Boolean isAccountfoundByContact = false;

                Case c = new Case();
                if(String.isNotBlank(AccId) && String.isNotEmpty(AccId))	c.AccountId         = AccId;
                if(String.isNotBlank(oppId) && String.isNotEmpty(oppId))	c.Opportunity__c 	= oppId;
                
                c.Contact_Email__c  = EmailId;
                c.Type              = InqueryReason;
                c.Repeat_Issue__c   = isChecked;
                c.Description       = Description;
                c.Origin            = 'Webform';
                c.Contact_Name__c 	= FirstName +' '+ LastName;
                c.Contact_Email__c 	= EmailId;
                c.Contact__c 		= PhoneNumber;
                c.Account_Name__c	= AccountName;
                c.Account_Number__c = AccountNumber;
                c.Submitted_By__c 	= Submittedby;
                c.An_urgent_request_for_immediate_assistan__c = Urgency;
                c.Preferred_method_of_contact__c = PrefferdMehtod;                
                insert c;
                SendEmailToOtherMail(c.Id,c.Contact_Email__c);
                fieldEmpty();
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Case has been created successfully!'));
                
             }catch(Exception e){
                 System.debug(e.getMessage());
                 System.debug(e.getLineNumber());
                 ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Something went wrong please try again !'+e.getMessage() + e.getLineNumber()));
             }
        }
        
    }
    
    public void fieldEmpty(){
        
        FirstName       = '';
        LastName        = '';
        AccountName     = '';
        AccountNumber   = '';
        EmailId         = '';
        PhoneNumber     = '';
        PrefferdMehtod  = '';
        InqueryReason   = '';
        Urgency         = '';
        Description     = '';
        isChecked       = false;
        Description     = '';
    }
    
    //Future method for send email to individual emails which have saved in an custom label.
    @future
    public static void SendEmailToOtherMail(String RecordId,String emailadd){
        
        String EmailString = Label.Emails_for_Notification_after_Webfrom_case_create;
        List<String> EmailList = EmailString.split(',');
        
        EmailTemplate et = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName = 'Email_to_Custom_Email_on_Create_new_case_from_webform'];
        
        Contact ct = [SELECT Id, Name, Email FROM Contact Limit 1];
                
        List<Messaging.SingleEmailMessage> EmailMessagingList = new List<Messaging.SingleEmailMessage>();
        
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setTemplateId(et.Id);
        mail.setWhatId(RecordId);
        mail.setToAddresses(EmailList);
        mail.setSaveAsActivity(false);
        mail.setTargetObjectId(ct.Id);
        mail.setTreatTargetObjectAsRecipient(false);

        EmailMessagingList.add(mail);
        
        
        Savepoint sp = Database.setSavepoint();
        try{
            if(EmailMessagingList.size() > 0){
                Messaging.sendEmail(EmailMessagingList);
            }
        }catch(Exception e){
            System.debug('Email error' +e.getMessage());
        }
        
        Database.rollback(sp);
        
        List<Messaging.SingleEmailMessage> msgListToBeSend = new List<Messaging.SingleEmailMessage>();
        
        for (Messaging.SingleEmailMessage email : EmailMessagingList) {
        
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getToAddresses());
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            msgListToBeSend.add(emailToSend);
        }
        
        Messaging.sendEmail(msgListToBeSend);
    }
    
    public void getPrefferedMethodList(){
        
        prefferedMethodList = getPicDynamic('Case', 'Preferred_method_of_contact__c');
    }
    
    public void getUrgencyList(){
        
        UrgencyTypeList = getPicDynamic('Case', 'An_urgent_request_for_immediate_assistan__c');
    }
    
    public void getInqueryList(){
        
        InqueryReasonsList = new List<SelectOption>();
        
        InqueryReasonsList.add(new SelectOption('','--None--'));
        InqueryReasonsList.add(new SelectOption('Billing Inquiry','Billing Inquiry'));
        InqueryReasonsList.add(new SelectOption('Change Request','Change Request'));
        InqueryReasonsList.add(new SelectOption('Cancellation','Cancellation'));
        InqueryReasonsList.add(new SelectOption('General Question','General Question'));
        InqueryReasonsList.add(new SelectOption('Complaint','Complaint'));
        InqueryReasonsList.add(new SelectOption('On Call Management','On Call Management'));
        InqueryReasonsList.add(new SelectOption('Portal Support','Portal Support'));
        InqueryReasonsList.add(new SelectOption('Secure Messaging/SMS','Secure Messaging/SMS'));
        InqueryReasonsList.add(new SelectOption('Other','Other'));
        
    }
    
    public List<SelectOption> getPicDynamic(String ObjectName, String FieldName){
        
        List<SelectOption> picklistValues = new List<SelectOption>();
        picklistValues.add(new SelectOption('','--None--'));
        
        SObjectType objectType = Schema.getGlobalDescribe().get(ObjectName);
        
        List<Schema.PicklistEntry> pick_list_values = objectType.getDescribe().fields.getMap().get(FieldName).getDescribe().getPickListValues();
        
        for(Schema.PicklistEntry aPickListValue : pick_list_values) {                   
            picklistValues.add(new SelectOption(aPickListValue.getLabel(),aPickListValue.getValue())); 
        }
        
        return picklistValues;
    }
}