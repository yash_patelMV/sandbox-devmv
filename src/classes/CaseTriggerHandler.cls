public class CaseTriggerHandler {
    
    List<Case> recordNewList = new List<Case>();
    List<Case> recordOldList = new List<Case>();
    Map<Id, Case> recordNewMap = new Map<Id, Case>();
    Map<Id, Case> recordOldMap = new Map<Id, Case>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    public static boolean recursionController = false;
    
	public CaseTriggerHandler(List<Case> newList, List<Case> oldList, Map<Id, Case> newMap, Map<Id, Case> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){
        CheckForBusinessHours(recordNewList);
    }
    
    // public void BeforeUpdateEvent(){}
    
    // public void BeforeDeleteEvent(){}
    
    public void AfterInsertEvent(){
        sendMailAfterBusinessHours(recordNewList);
    }
    
    public void AfterUpdateEvent(){
        sendMailOnStatusChange(recordNewList);
    }
    
    // public void AfterDeleteEvent(){}
    
    // public void AfterUndeleteEvent(){}
    
    
    //Check for business hours for further process
    public void CheckForBusinessHours(List<Case> CaseNewList){
    
        List<Messaging.SingleEmailMessage> EmailMessagingList = new List<Messaging.SingleEmailMessage>();
        
        Group ManagerQueue = [SELECT Id, DeveloperName from Group where Type = 'Queue' AND DeveloperName = 'Manager_Queue'];
        
        List<Id> OppId = new List<Id>();
        
        for(Case c : CaseNewList){
            OppId.add(c.Opportunity__c);
        }
        
        Map<Id, Opportunity> OpportunityMap = new Map<Id, Opportunity>([SELECT Id, Name, Account_Executive__c  FROM Opportunity WHERE Id IN : OppId]);
        
        BusinessHours bt = [SELECT Id FROM BusinessHours WHERE Name = 'Ansafone'];
        
        // Case Owner assignment according to their parent opportunity
        for(Case c : CaseNewList){
            
            DateTime CreatedDate = System.now();
            
            if(OpportunityMap.containsKey(c.Opportunity__c)){
                if(String.isNotBlank(OpportunityMap.get(c.Opportunity__c).Account_Executive__c)){  // && BusinessHours.isWithin(bt.Id,CreatedDate)
                    c.OwnerId = OpportunityMap.get(c.Opportunity__c).Account_Executive__c;
                }else{
                     c.OwnerId = ManagerQueue.Id;
                }
            }else{
                c.OwnerId = ManagerQueue.Id;
            }
            
            if(!BusinessHours.isWithin(bt.Id,CreatedDate)){
                c.Not_in_business_hour__c = true;
            }
            
        }
    
    }
    
    //Email to contact on status change
    public void sendMailOnStatusChange(List<Case> CaseNewList){
        List<Messaging.SingleEmailMessage> EmailMessagingList = new List<Messaging.SingleEmailMessage>();
        
        Group ManagerQueue = [SELECT Id, DeveloperName from Group where Type = 'Queue' AND DeveloperName = 'Manager_Queue'];
        
        Map<String,Id> EmailTemplateMap = new Map<String, Id>();
        
        List<String> templateList = new List<String>{   'Case_Status_Change_In_Process_Notification_to_Contact',
                                                        'Case_Status_Change_Open_Notification_to_Contact', 
                                                        'Case_Status_Change_Escalated_Notification_to_Contact',
                                                        'Case_Status_Change_Resolved_Notification_to_Contact',
                                                        'Case_Status_Change_Pending_Info_Notification_to_Contact'
                                                    };
        
        for(EmailTemplate et : [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName IN: templateList]){
            EmailTemplateMap.put(et.DeveloperName, et.Id);
        }
        
        for(Case c : CaseNewList){
            
            String emailTemplate = '';
            Boolean issend = false;
            
            // if(recordOldMap.get(c.Id).Status == recordNewMap.get(c.Id).Status && c.Send_Email_with_Status_Change__c == true ){
            //     sendMailAfterBusinessHours(recordNewList);
            // }

            if(recordOldMap.get(c.Id).Status != recordNewMap.get(c.Id).Status && c.Status == 'In Process' && c.Send_Email_with_Status_Change__c == true && c.Reason != 'Internal Communication' && c.Reason != 'Sales Support' && c.Reason != 'Program Implementation' && c.Type != 'Internal Communication' && c.Type != 'Sales Support' && c.Type != 'Program Implementation'){
                issend = true;
                emailTemplate = 'Case_Status_Change_In_Process_Notification_to_Contact';
            }
            
            if(recordOldMap.get(c.Id).Status != recordNewMap.get(c.Id).Status && c.Status == 'Open' && c.Send_Email_with_Status_Change__c == true && c.Reason != 'Internal Communication' && c.Reason != 'Sales Support' && c.Reason != 'Program Implementation' && c.Type != 'Internal Communication' && c.Type != 'Sales Support' && c.Type != 'Program Implementation'){
                issend = true;
                emailTemplate = 'Case_Status_Change_Open_Notification_to_Contact';
            }

            if(recordOldMap.get(c.Id).Status != recordNewMap.get(c.Id).Status && c.Status == 'Escalated' && c.Send_Email_with_Status_Change__c == true && c.Reason != 'Internal Communication' && c.Reason != 'Sales Support' && c.Reason != 'Program Implementation' && c.Type != 'Internal Communication' && c.Type != 'Sales Support' && c.Type != 'Program Implementation'){
                issend = true;
                emailTemplate = 'Case_Status_Change_Escalated_Notification_to_Contact';
            }
            
            if(recordOldMap.get(c.Id).Status != recordNewMap.get(c.Id).Status && c.Status == 'Resolved' && c.Send_Email_with_Status_Change__c == true && c.Reason != 'Internal Communication' && c.Reason != 'Sales Support' && c.Reason != 'Program Implementation' && c.Type != 'Internal Communication' && c.Type != 'Sales Support' && c.Type != 'Program Implementation'){
                issend = true;
                emailTemplate = 'Case_Status_Change_Resolved_Notification_to_Contact';
            }
            
            if(recordOldMap.get(c.Id).Status != recordNewMap.get(c.Id).Status && c.Status == 'Pending Info Client' && c.Send_Email_with_Status_Change__c == true && c.Reason != 'Internal Communication' && c.Reason != 'Sales Support' && c.Reason != 'Program Implementation' && c.Type != 'Internal Communication' && c.Type != 'Sales Support' && c.Type != 'Program Implementation'){
                issend = true;
                emailTemplate = 'Case_Status_Change_Pending_Info_Notification_to_Contact';
            }
            
            if(issend && String.isNotBlank(c.ContactId) && String.isNotEmpty(c.ContactId)){
            
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setTargetObjectId(c.ContactId);
                message.setWhatId(c.Id);
      			//message.setOrgWideEmailAddressId('0D2P00000008Qfy');
                message.setTemplateId(EmailTemplateMap.get(emailTemplate));
                message.setSaveAsActivity(true);
                
                EmailMessagingList.add(message);
            }

        }
        
        System.debug('EmailMessagingList.size('+EmailMessagingList.size());
        
        try{
            if(EmailMessagingList.size() > 0){
                Messaging.sendEmail(EmailMessagingList);
            }
        }catch(Exception e){
            System.debug('Email error' +e.getMessage());
        }
    }
    

    //Email to contact on new case arrival
    public void sendMailAfterBusinessHours(List<Case> CaseNewList){
        
        List<Messaging.SingleEmailMessage> EmailMessagingList = new List<Messaging.SingleEmailMessage>();
        
        Map<String,Id> EmailTemplateMap = new Map<String, Id>();
        
        List<String> templateList = new List<String>{'Case_New_case_after_business_hours_notify_client', 'Case_New_case_in_business_hours_notify_client'};
        
        for(EmailTemplate et : [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName IN: templateList]){
            EmailTemplateMap.put(et.DeveloperName, et.Id);
        }
        
        BusinessHours bt = [SELECT Id FROM BusinessHours WHERE Name = 'Ansafone'];

        for(Case c : CaseNewList){
            
            if(c.ContactId != null && c.Send_Email_with_Status_Change__c == true){
                
                if(c.Type !='Sales Support' && c.Type != 'Internal Communications' && c.Type != 'Program Implementation'){
                    
                    DateTime CreatedDate = c.CreatedDate;
                    
                    System.debug('CreatedDate'+CreatedDate);
                    
                    String emailTemplate = '';
                    
                    System.debug('bt'+bt);

                    Boolean isWithin= BusinessHours.isWithin(bt.id, CreatedDate);
                
                    System.debug('isWithin'+isWithin);

                    if(!BusinessHours.isWithin(bt.Id,CreatedDate)){
                        emailTemplate = 'Case_New_case_after_business_hours_notify_client';
                    }else{
                        emailTemplate = 'Case_New_case_in_business_hours_notify_client';
                    }
                    
                    if(String.isNotBlank(emailTemplate) && String.isNotEmpty(emailTemplate)){
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    
                        message.setTargetObjectId(c.ContactId);
                        message.setWhatId(c.Id);
                        //message.setOrgWideEmailAddressId('0D2P00000008Qfy');
                        message.setTemplateId(EmailTemplateMap.get(emailTemplate));
                        message.setSaveAsActivity(true);
                    
                        EmailMessagingList.add(message);
                    }
                }
            }
        }
        
        if(EmailMessagingList.size() > 0){
            Messaging.sendEmail(EmailMessagingList);
        }
    }
}