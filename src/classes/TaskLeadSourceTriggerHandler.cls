public class TaskLeadSourceTriggerHandler {
    
    List<Task> recordNewList = new List<Task>();
    List<Task> recordOldList = new List<Task>();
    Map<Id, Task> recordNewMap = new Map<Id, Task>();
    Map<Id, Task> recordOldMap = new Map<Id, Task>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    public static boolean recursionController = false;
    
    public TaskLeadSourceTriggerHandler(List<Task> newList, List<Task> oldList, Map<Id, Task> newMap, Map<Id, Task> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){
        updateLeadSource();
    }
    
    public void BeforeUpdateEvent(){
        updateLeadSource();
    }
    
    public void BeforeDeleteEvent(){}
    
    public void AfterInsertEvent(){
    }
    
    public void AfterUpdateEvent(){
    }
    
    public void AfterDeleteEvent(){}
    
    public void AfterUndeleteEvent(){}
    
    public void updateLeadSource(){
        
        Set<Id>leadId = new Set<Id>();
        Set<Id>conId = new Set<Id>();
        for(Task tsk: recordNewList){
             if(tsk.WhoId != null){
              String s1 = tsk.WhoId;
               if(s1.left(3) == '00Q'){
                    leadId.add(tsk.WhoId);
               }
               if(s1.left(3) == '003'){
                   conId.add(tsk.WhoId);
               }
             }
           
        }
        List<Lead> LdList = [select id,LeadSource from Lead where id =: leadId];
        List<Contact> conList = [select id,LeadSource from Contact where id =: conId];
        
        
        for(Task t : recordNewList){
            if(t.WhoId != null){
                String s2 = t.WhoId;
                if(s2.left(3) == '00Q'){
                    if(LdList.size() > 0){
                        for(Lead ld : LdList){
                            t.Object__c = 'Lead';
                            t.LeadSource__c = ld.LeadSource;
                        }
                    }
                }
                 if(s2.left(3) == '003'){
                    if(conList.size() > 0){
                            for(Contact con : conList){
                                t.Object__c = 'Contact';
                                t.LeadSource__c = con.LeadSource;
                            }
                        }
                    }
            }
        }
    }
        
}