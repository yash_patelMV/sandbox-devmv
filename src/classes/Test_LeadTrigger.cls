@isTest
private class Test_LeadTrigger {

	private static testMethod void test() {
	    
	    insert new Duplicate_Notify_User__c(Name='Testing', First_Name__c = 'Lalit', Last_Name__c = 'test', Email__c = 'test@test.com');

        Lead l = new Lead(FirstName = 'test', LastName='test', Email='Test@test.com', Company='MV Clouds Pvt. Ltd.');
        insert l;
        
        Lead l2 = new Lead(FirstName = 'test', LastName='test', Email='Test@test.com', Company='MV Clouds Pvt. Ltd.');
        
        Test.startTest();
         insert l2;
         update l;
         delete l;
         undelete l;
        Test.stopTest();
	}

}