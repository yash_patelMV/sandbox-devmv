public class LeadTriggerhandler {
    
    List<Lead> recordNewList = new List<Lead>();
    List<Lead> recordOldList = new List<Lead>();
    Map<Id, Lead> recordNewMap = new Map<Id, Lead>();
    Map<Id, Lead> recordOldMap = new Map<Id, Lead>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    public static boolean recursionController = false;
    
    public LeadTriggerhandler(List<Lead> newList, List<Lead> oldList, Map<Id, Lead> newMap, Map<Id, Lead> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){}
    
    public void BeforeUpdateEvent(){}
    
    public void BeforeDeleteEvent(){}
    
    public void AfterInsertEvent(){
    }
    
    public void AfterUpdateEvent(){
        updateLead();
    }
    
    public void AfterDeleteEvent(){}
    
    public void AfterUndeleteEvent(){}
    
    public void updateLead(){
        Set<Id>ids = new Set<Id>();
        
        
        for(Lead ld : recordNewList){
            Lead oldLead = recordOldMap.get(ld.Id);
            if(ld.LeadSource != oldLead.LeadSource){
                ids.add(ld.Id);
            }
        }
        List<Task>tskList = new List<Task>();
        List<Task>taskList = [SELECT Id,WhoId,LeadSource__c FROM Task WHERE WhoId =: ids];
        Map<Id,List<Task>>tkMap = new Map<Id,List<Task>>();
        for(Task t : taskList){
            if(!tkMap.containsKey(t.WhoId)){
                List<Task>TasList = new List<Task>();
                TasList.add(t);
                tkMap.put(t.WhoId,TasList);
            }
            else{
                List<Task>TasList =tkMap.get(t.WhoId);
                TasList.add(t);
                tkMap.put(t.WhoId,TasList);
            }
        }
        
        for(Id ldId : tkMap.keySet()){
            Lead oldLead = recordOldMap.get(ldId);
            Lead newLead = recordNewMap.get(ldId);
            if(newLead.LeadSource != oldLead.LeadSource){
                if(taskList.size()>0){
                    for(Task ts: tkMap.get(ldId)){
                        if(newLead.LeadSource != ts.LeadSource__c){
                            Task tsk = new Task();
                            tsk.Id = ts.Id;
                            tsk.LeadSource__c = newLead.LeadSource;
                            tsk.Object__c = 'Lead';
                            tskList.add(tsk);
                        }
                    }
                
                }
            }
            
        } 
        update tskList;
    }

}