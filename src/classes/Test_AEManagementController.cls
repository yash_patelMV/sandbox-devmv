@isTest
private class Test_AEManagementController {
    @isTest
	private static void test() {
	    Account acc = new Account(name='test');
        insert acc;
        
	    Opportunity opp = new Opportunity(Name = 'test', StageName = 'Prospecting', CloseDate = System.today(), AccountId = acc.Id,Account_Executive__c = UserInfo.getUserId());
	    insert opp;
	    
        AEManagementController ae = new AEManagementController();
        ae.selecteduserId = UserInfo.getUserId();
        ae.searchOpp = 'te';
        ae.getData();
        ae.searchOpportunity();
        ae.getListOfUser();
        ae.selectAll();
        ae.addExecutive();
        
	}
}