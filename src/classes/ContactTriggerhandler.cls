public class ContactTriggerhandler {
    
    List<Contact> recordNewList = new List<Contact>();
    List<Contact> recordOldList = new List<Contact>();
    Map<Id, Contact> recordNewMap = new Map<Id, Contact>();
    Map<Id, Contact> recordOldMap = new Map<Id, Contact>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    public static boolean recursionController = false;
    
    public ContactTriggerhandler(List<Contact> newList, List<Contact> oldList, Map<Id, Contact> newMap, Map<Id, Contact> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){}
    
    public void BeforeUpdateEvent(){}
    
    public void BeforeDeleteEvent(){}
    
    public void AfterInsertEvent(){
    }
    
    public void AfterUpdateEvent(){
        updateContact();
    }
    
    public void AfterDeleteEvent(){}
    
    public void AfterUndeleteEvent(){}
    
    public void updateContact(){
        Set<Id>ids = new Set<Id>();
        
        for(Contact con : recordNewList){
            Contact oldcon = recordOldMap.get(con.Id);
            if(con.LeadSource != oldcon.LeadSource){
                ids.add(con.Id);
            }
        }
        
        List<Task>tskList = new List<Task>();
        List<Task>taskList = [SELECT Id,WhoId,LeadSource__c FROM Task WHERE WhoId =: ids];
        
        Map<Id,List<Task>>tkMap = new Map<Id,List<Task>>();
        for(Task t : taskList){
            if(!tkMap.containsKey(t.WhoId)){
                List<Task>TasList = new List<Task>();
                TasList.add(t);
                tkMap.put(t.WhoId,TasList);
            }
            else{
                List<Task>TasList = tkMap.get(t.WhoId);
                TasList.add(t);
                tkMap.put(t.WhoId,TasList);
            }
        }
        
        for(Id conId : tkMap.keySet()){
            Contact oldcon = recordOldMap.get(conId);
            Contact newcon = recordNewMap.get(conId);
            if(newcon.LeadSource != oldcon.LeadSource){
                if(taskList.size() > 0){
                    for(Task ts: tkMap.get(conId)){
                        if(newcon.LeadSource != ts.LeadSource__c){
                            Task tsk = new Task();
                            tsk.Id = ts.Id;
                            tsk.LeadSource__c = newcon.LeadSource;
                            tsk.Object__c = 'Contact';
                            tskList.add(tsk);
                        }
                    }
                }
            } 
        }
        update tskList;
    }

}