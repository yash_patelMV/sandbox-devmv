public  class leadConversionReportController {
public  Map<Id,Integer>leadMap{get;set;}
public  Map<Id,Integer>AllLeadMap1{get;set;}
public  Map<Id,Integer>AllLeadMap2{get;set;}
public Map<Id,Integer>LeadMap1{get;set;}
public Map<Id,Integer>LeadMap2{get;set;}
public Map<Id,Integer>LeadMap3{get;set;}
public  Map<Id,Integer>TaskMap{get;set;}
public  Map<Id,Integer>TaskMap1{get;set;}
public  Map<Id,Integer>EmailMap1{get;set;}
public  Map<Id,Integer>EmailMap2{get;set;}
public  Map<Id,List<Event>>EvtMap{get;set;}
public  Map<Id,Integer>leadMapdata{get;set;}
public  Map<Id,Decimal>OppMap{get;set;}
public Map<Id,Decimal>UpsellMap1{get;set;}
public Map<Id,Decimal>UpsellMap2{get;set;}
public Map<Id,Decimal>UpsellMap3{get;set;}
public Map<Id,Decimal>UpsellMap4{get;set;}
public Map<Id,Decimal>UpsellMap5{get;set;}
public Map<Id,Decimal>UpsellMap6{get;set;}
public  Map<Id,Decimal>OppMap1{get;set;}
public  Map<Id,Decimal>OppMap2{get;set;}
public  Map<Id,Decimal>OppMap3{get;set;}
public  Map<Id,Decimal>OppMap4{get;set;}
public  Map<Id,Decimal>OppMap5{get;set;}
public  Map<Id,Decimal>OppMap6{get;set;}
public  Map<Id,Decimal>OppMap0{get;set;}
public  Map<Id,Decimal>OptAmount1{get;set;}
public  Decimal oppMapSize0{get;set;}
public  Decimal oppMapSize{get;set;}
public  Decimal oppMapSize1{get;set;}
public  Decimal oppMapSize2{get;set;}
public  Decimal oppMapSize3{get;set;}
public  Decimal oppMapSize4{get;set;}
public  Decimal oppMapSize5{get;set;}
public  Decimal oppMapSize6{get;set;}
public  Decimal oppMapamtSize{get;set;}
public  Decimal UpsellMapSize1{get;set;}
public  Decimal UpsellMapSize2{get;set;}
public  Decimal UpsellMapSize3{get;set;}
public  Decimal UpsellMapSize4{get;set;}
public  Decimal UpsellMapSize5{get;set;}
public  Decimal UpsellMapSize6{get;set;}
public  Decimal TaskSize{get;set;}
public  Decimal AllLeadSize1 {get;set;}
public  Decimal AllLeadSize2 {get;set;}
public  Decimal leadSize {get;set;}
public  Decimal leadSize4 {get;set;}
public  Decimal leadSize5 {get;set;}
public  Decimal leadSize6 {get;set;}
public  Decimal leadSize1 {get;set;}
public  Decimal EmailSize1 {get;set;}
public  Decimal EmailSize2 {get;set;}
public  Decimal TaskSize1{get;set;}
public  Decimal ldCount{get;set;}
public  Decimal ConvertedCount{get;set;}
public  string ChkMapKey{get;set;}
public  string ChkMapKey1{get;set;}
public  string EmlMapKey1{get;set;}
public  string EmlMapKey2{get;set;}
public  string opMapKey0{get;set;}
public  string opMapKey{get;set;}
public  string opMapKey1{get;set;}
public  string opMapKey2{get;set;}
public  string opMapKey3{get;set;}
public  string opMapKey4{get;set;}
public  string opMapKey5{get;set;}
public  string opMapKey6{get;set;}
public String opMapKeyamt{get;set;}
public  string UpsellMapKey1{get;set;}
public  string UpsellMapKey2{get;set;}
public  string UpsellMapKey3{get;set;}
public  string UpsellMapKey4{get;set;}
public  string UpsellMapKey5{get;set;}
public  string UpsellMapKey6{get;set;}
public  string aldMapKey1{get;set;}
public  string aldMapKey2{get;set;}
public  string lddMapKey{get;set;}
public  string lddMapKey1{get;set;}
public  string lddMapKey4{get;set;}
public  string lddMapKey5{get;set;}
public  string lddMapKey6{get;set;}
public String UserName{get;set;}
public  Date datobj{get;set;}
public  Integer dy{get;set;}
public  Integer mon{get;set;}
public  Integer yr{get;set;}
public  Map<Id,User> UserListName {get;set;}
public  String reportId {get;set;}
public  String reportId1 {get;set;}
public  String reportId2 {get;set;}
// public Map<Id,leaddata>leadMap {get;set;}
    public  leadConversionReportController(){
        leadMap = new Map<Id,Integer>();
        AllLeadMap1 = new Map<Id,Integer>();
        AllLeadMap2 = new Map<Id,Integer>();
        LeadMap1 = new Map<Id,Integer>();
        LeadMap2 = new Map<Id,Integer>();
        LeadMap3 = new Map<Id,Integer>();
        //  conversionSDRList = new List<leaddata>();
        leadMapdata = new Map<Id,Integer>();
        TaskMap = new  Map<Id,Integer>();
        TaskMap1 = new  Map<Id,Integer>();
        EmailMap1 = new  Map<Id,Integer>();
        EmailMap2 = new  Map<Id,Integer>();
        EvtMap = new  Map<Id,list<Event>>();
        OppMap =  new Map<Id,Decimal>();
        OppMap1 =  new Map<Id,Decimal>();
        OppMap2 =  new Map<Id,Decimal>();
        OppMap3 =  new Map<Id,Decimal>();
        OppMap4 = new Map<Id,Decimal>();
        OppMap5 = new Map<Id,Decimal>();
        OppMap6 = new Map<Id,Decimal>();
        OppMap0 = new Map<Id,Decimal>();
        OptAmount1 = new Map<Id,Decimal>();
        UpsellMap1 = new Map<Id,Decimal>();
        UpsellMap2 = new Map<Id,Decimal>();
        UpsellMap3 = new Map<Id,Decimal>();
        UpsellMap4 = new Map<Id,Decimal>();
        UpsellMap5 = new Map<Id,Decimal>();
        UpsellMap6 = new Map<Id,Decimal>();
        UserListName = new Map<Id,User>();
        
         Set<Id>ids= new Set<Id>();
        for(User us :[SELECT Id,Name FROM User WHERE Name IN('Elizabeth Healy','Aleen Schiavo','Jamie Portella','Tim Clarke','Steve Garnham','Gary Reno','Ricky Morse') ]){
             ids.add(us.Id);
            UserListName.put(us.Id,us);
            System.debug('Name'+us.Name);
        }
        UserName = UserInfo.getName();
        
        List<AggregateResult> AggregateResultList1 = [SELECT OwnerId,Count(Id)ld1 FROM Lead WHERE Status NOT IN ('Dead - Bad Data-number_email', 'Dead-DRTV','Dead-Outbound Too Small',
                                                             'Dead-Looking for a job','Dead - Never Responded','Dead- They are a call center','Dead - They are not looking for service'
                                                            ,'Operations','Spam') AND LeadSource NOT IN ('Uplead','Cold Calling','No source provided') AND OwnerId =: ids GROUP BY OwnerId];
        List<AggregateResult> AggregateResultList2 = [SELECT OwnerId,Count(Id)ld2 FROM Lead WHERE IsConverted = true AND Status NOT IN ('Dead - Bad Data-number_email', 'Dead-DRTV',
                                                            'Dead-Outbound Too Small','Dead-Looking for a job','Dead - Never Responded','Dead- They are a call center','Dead - They are not looking for service'
                                                            ,'Operations','Spam') AND LeadSource NOT IN ('Uplead','Cold Calling','No source provided') AND OwnerId =: ids GROUP BY OwnerId];
                                                            
        List<AggregateResult> ldList1 = [SELECT OwnerId,Count(Id)ldd1 FROM Lead WHERE LastActivityDate <=: System.today()-2 AND LastActivityDate >: System.today()-5 AND  LeadSource NOT IN ('Uplead','Cold Calling','No source provided')  AND OwnerId =: ids GROUP BY OwnerId];
        List<AggregateResult> ldList2 = [SELECT OwnerId,Count(Id)ldd2 FROM Lead WHERE LastActivityDate <=: System.today()-5 AND LastActivityDate >: System.today()-10 AND LeadSource NOT IN ('Uplead','Cold Calling','No source provided') AND OwnerId =: ids  GROUP BY OwnerId];
        List<AggregateResult> ldList3 = [SELECT OwnerId,Count(Id)ldd3 FROM Lead WHERE LastActivityDate <=: System.today()-10 AND LeadSource NOT IN ('Uplead','Cold Calling','No source provided') AND OwnerId =: ids  GROUP BY OwnerId];
                                                       
                                                        //   WHERE Name IN('Elizabeth Healy','Aleen Schiavo','Jamie Portella','Tim Clarke','Steve Garnham','Gary Reno','Ricky Morse')  
        leadSize4 = 0;
        for(AggregateResult ld : ldList1){
            Integer leadListdata1 = LeadMap1.get((id)ld.get('OwnerId'));
            if(leadListdata1 == null)
                LeadMap1.put((id)ld.get('OwnerId'),(Integer)ld.get('ldd1'));
                lddMapKey4 = string.valueof(LeadMap1.keyset());
        }
        leadSize4 = LeadMap1.size();
       
        leadSize5 = 0;
         for(AggregateResult ld1: ldList2){
            Integer leadListdata2 = LeadMap2.get((id)ld1.get('OwnerId'));
            if(leadListdata2 == null)
                LeadMap2.put((id)ld1.get('OwnerId'),(Integer)ld1.get('ldd2'));
                lddMapKey5 = string.valueof(LeadMap2.keyset());
        }
        leadSize5 = LeadMap2.size();
        
        leadSize6 = 0;
         for(AggregateResult ld2: ldList3){
            Integer leadListdata3 = LeadMap3.get((id)ld2.get('OwnerId'));
            if(leadListdata3 == null)
                LeadMap3.put((id)ld2.get('OwnerId'),(Integer)ld2.get('ldd3'));
                lddMapKey6 = string.valueof(LeadMap3.keyset());
        }
        leadSize6 = LeadMap3.size();                                                  
                                                            
        leadSize1 = 0;
        for(AggregateResult l: AggregateResultList1){
             Integer leadListdata = leadMapdata.get((id)l.get('OwnerId'));
             if(leadListdata == null)
                leadMapdata.put((id)l.get('OwnerId'),(Integer)l.get('ld1'));
                lddMapkey1 = string.valueof(leadMapdata.keyset());
        }
        leadSize1 = leadMapdata.size();
        
        leadSize = 0;
        for(AggregateResult ld : AggregateResultList2){
             Integer leadListdata4 = leadMap.get((id)ld.get('OwnerId'));
            if(leadListdata4 == null)
                leadMap.put((id)ld.get('OwnerId'),(Integer)ld.get('ld2'));
                lddMapKey = string.valueof(leadMap.keyset());
                
        }
        leadSize = leadMap.size();
        List<String> assignedValues = new List<String>{'gill','Huirse','bryant','gibbs','Hall','Brumett','Bhavsar','Kurzhals','Bralish','Fuller','Wollschlager','Thomas','Villatuya','Johnson','General'};

        List<AggregateResult>taskList = [SELECT Count(Id)tsk,OwnerId FROM Task WHERE  Status = 'Completed' AND Subject NOT IN ('Open emails',' Mass email blast')
                                     AND (NOT Owner.Name  LIKE : assignedValues) AND OwnerId =: ids AND ActivityDate = TODAY GROUP BY OwnerId];
        TaskSize = 0;
        if(taskList.size() > 0){
            for(AggregateResult t : taskList){
                 Integer tskList = TaskMap.get((id)t.get('OwnerId'));
                 if(tskList == null)
                    TaskMap.put((id)t.get('OwnerId'),(Integer)t.get('tsk'));
                    ChkMapKey = string.valueof(TaskMap.keyset());
            }
        }
        TaskSize = TaskMap.size();
        // List<Event>taskList1= [SELECT ActivityDate,Description,Subject,OwnerId FROM Event WHERE  Subject NOT IN ('Open emails',' Mass email blast') 
        //                               AND OwnerId =: ids AND ActivityDate = TODAY LIMIT 10000];
        // TaskSize1 = 0;
        // if(taskList1.size() > 0){
        //     for(Event e : taskList1){
        //          List<Event> tskList1 = EvtMap.get(e.OwnerId);
        //          if(tskList1 == null)
        //             tskList1 = new List<Event>();
        //             tskList1.add(e);
        //             EvtMap.put(e.OwnerId,tskList1);
        //     }
        // }
        // TaskSize1 = EvtMap.size();
        List<AggregateResult> opplist= [SELECT OwnerId,Count(Id)op
                                        FROM   Opportunity 
                                        WHERE StageName IN ('Needs Analysis','Proposal/Price Quote','Negotiation','Qualified & Active','Negotiation/Review - Contract Sent','Site Visit',
                                              'Proposal/RFP out','Contract/Proposal/RFP out') And  OwnerId =: ids GROUP BY OwnerId];
       List<AggregateResult> opplist0= [SELECT OwnerId,Sum(Amount)oppp1 
                                        FROM   Opportunity 
                                        WHERE StageName IN ('Needs Analysis','Proposal/Price Quote','Negotiation','Qualified & Active','Negotiation/Review - Contract Sent','Site Visit',
                                              'Proposal/RFP out','Contract/Proposal/RFP out') And  OwnerId =: ids GROUP BY OwnerId];
                                              
        oppMapSize = 0;
        if(oppList.size()>0){
            for(AggregateResult opp: opplist){
                Decimal oppListdata = OppMap.get((id)opp.get('OwnerId'));
                if(oppListdata == null)
                    OppMap.put((id)opp.get('OwnerId'),(Decimal)opp.get('op'));
                    opMapKey = string.valueof(OppMap.keyset());
                }
                oppMapSize = OppMap.size();
        }
        
        oppMapSize0 = 0;
        if(oppList0.size()>0){
            for(AggregateResult opp0: opplist0){
                Decimal oppListdata0 = OppMap0.get((id)opp0.get('OwnerId'));
                if(oppListdata0 == null)
                    OppMap0.put((id)opp0.get('OwnerId'),(Decimal)opp0.get('oppp1'));
                    opMapKey0 = string.valueof(OppMap0.keyset());
                    System.debug('opMapKey0'+opMapKey0);
                }
                oppMapSize0 = OppMap0.size();
        }
        
        List<AggregateResult> opplist1= [SELECT OwnerId,Count(Id)op1,Sum(Amount)amt1 FROM Opportunity WHERE StageName IN ('Closed Won','Closed Won Project') AND OwnerId =: ids GROUP BY OwnerId];
        oppMapSize1 = 0;
        if(opplist1.size()>0){
            for(AggregateResult opp1: opplist1){
                Decimal oppListdata1 = OppMap1.get((id)opp1.get('OwnerId'));
                if(oppListdata1 == null)
                    OppMap1.put((id)opp1.get('OwnerId'),(Decimal)opp1.get('op1'));
                    opMapKey1 = string.valueof(OppMap1.keyset());
                }
                oppMapSize1 = OppMap1.size();
        }
        
        List<AggregateResult> opplistamt= [SELECT OwnerId,Sum(Amount)amt1 FROM Opportunity WHERE StageName IN ('Closed Won','Closed Won Project') AND OwnerId =: ids GROUP BY OwnerId];
        oppMapamtSize = 0;
        if(opplistamt.size()>0){
            for(AggregateResult oppamt: opplistamt){
                Decimal oppListdataamt = OptAmount1.get((id)oppamt.get('OwnerId'));
                if(oppListdataamt == null)
                    OptAmount1.put((id)oppamt.get('OwnerId'),(Decimal)oppamt.get('amt1'));
                    opMapKeyamt = string.valueof(OptAmount1.keyset());
                }
                oppMapamtSize = OptAmount1.size();
        }

        List<AggregateResult> opplist2= [SELECT OwnerId,Sum(Amount)op2 FROM Opportunity WHERE StageName = 'Closed Won' AND OwnerId =: ids GROUP BY OwnerId];
        oppMapSize2 = 0;
        System.debug('opplist2'+opplist2.size());
        if(opplist2.size()>0){
             for(AggregateResult opp2: opplist2){
                Decimal oppListdata2 = OppMap2.get((id)opp2.get('OwnerId'));
                if(oppListdata2 == null)
                    OppMap2.put((id)opp2.get('OwnerId'),(Decimal)opp2.get('op2'));
                    opMapKey2 = string.valueof(OppMap2.keyset());
                }
                oppMapSize2 = OppMap2.size();
        }
        
        List<AggregateResult> opplist3= [SELECT OwnerId,Sum(Amount)op3 FROM Opportunity WHERE StageName = 'Project-Closed Won' AND OwnerId =: ids GROUP BY OwnerId];
        oppMapSize3 = 0;
        if(opplist3.size()>0){
            for(AggregateResult opp3: opplist3){
                Decimal oppListdata3 = OppMap3.get((id)opp3.get('OwnerId'));
                if(oppListdata3 == null)
                    OppMap3.put((id)opp3.get('OwnerId'),(Decimal)opp3.get('op3'));
                    opMapKey3 = string.valueof(OppMap3.keyset());
                }
                oppMapSize3 = OppMap3.size();
        }
        
        
        List<AggregateResult> opplist4= [SELECT OwnerId,Count(Id)op4 FROM Opportunity 
                                         WHERE LastActivityDate <=:System.today()-2 AND LastActivityDate >:System.today()-5 AND  OwnerId =: ids  GROUP BY OwnerId];
        oppMapSize4 = 0;
        if(oppList4.size()>0){
            for(AggregateResult opp4: opplist4){
                Decimal oppListdata4 = OppMap4.get((id)opp4.get('OwnerId'));
                if(oppListdata4 == null)
                    OppMap4.put((id)opp4.get('OwnerId'),(Decimal)opp4.get('op4'));
                    opMapKey4 = string.valueof(OppMap4.keyset());
                }
                oppMapSize4 = OppMap4.size();
        }
        
        List<AggregateResult> opplist5= [SELECT OwnerId,Count(Id)op5 FROM Opportunity
                                         WHERE LastActivityDate <=:System.today()-5 AND LastActivityDate >:System.today()-10 AND OwnerId =: ids  GROUP BY OwnerId];
        oppMapSize5 = 0;
        if(oppList5.size()>0){
            for(AggregateResult opp5: opplist5){
                Decimal oppListdata5 = OppMap5.get((id)opp5.get('OwnerId'));
                if(oppListdata5 == null)
                    OppMap5.put((id)opp5.get('OwnerId'),(Decimal)opp5.get('op5'));
                    opMapKey5 = string.valueof(OppMap5.keyset());
                }
                oppMapSize5 = OppMap5.size();
        
        }
        
        List<AggregateResult> opplist6= [SELECT OwnerId,Count(Id)op6 FROM Opportunity WHERE LastActivityDate <=:System.today()-10 AND  OwnerId =: ids GROUP BY OwnerId];
        oppMapSize6 = 0;
        if(oppList6.size()>0){
            for(AggregateResult opp6: opplist6){
                Decimal oppListdata6 = OppMap6.get((id)opp6.get('OwnerId'));
                if(oppListdata6 == null)
                    OppMap6.put((id)opp6.get('OwnerId'),(Decimal)opp6.get('op6'));
                    opMapKey3 = string.valueof(OppMap3.keyset());
                }
                oppMapSize6 = OppMap6.size();
        
        }
        
        List<AggregateResult> AllLeadList = [SELECT OwnerId,Count(Id)ald1 FROM Lead WHERE  LeadSource  IN ('Uplead','Cold Calling') AND OwnerId =: ids GROUP BY OwnerId];
        AllLeadSize1 = 0;
        if(AllLeadList.size() > 0){
            for(AggregateResult ld : AllLeadList){
                Decimal leadListdata3 = AllLeadMap1.get((id)ld.get('OwnerId'));
                if(leadListdata3 == null)
                    AllLeadMap1.put((id)ld.get('OwnerId'),(Integer)ld.get('ald1'));
                    aldMapKey1 = string.valueof(AllLeadMap1.keyset());
            }
        }
        AllLeadSize1= AllLeadMap1.size();
        List<AggregateResult> AllLeadList1 = [SELECT OwnerId,Count(Id)ald1 FROM Lead WHERE  LeadSource  IN ('Uplead','Cold Calling') AND IsConverted = true AND OwnerId =: ids GROUP BY OwnerId];
        AllLeadSize2 = 0;
        if(AllLeadList1.size() > 0){
            for(AggregateResult ld : AllLeadList1){
                Integer leadListdata4 = AllLeadMap2.get((id)ld.get('OwnerId'));
                if(leadListdata4 == null)
                    AllLeadMap2.put((id)ld.get('OwnerId'),(Integer)ld.get('ald1'));
                    aldMapKey2 = string.valueof(AllLeadMap2.keyset());
            }
        }
        AllLeadSize2= AllLeadMap2.size();
        
        List<AggregateResult>taskList1 = [SELECT Count(Id)tsk1, OwnerId FROM Task WHERE Subject Like '%call%' AND OwnerId =: ids GROUP BY OwnerId];
        TaskSize1 = 0;
        if(taskList1.size() > 0){
            for(AggregateResult t1 : taskList1){
                 Integer tskList2 = TaskMap1.get((id)t1.get('OwnerId'));
                 if(tskList2 == null)
                    TaskMap1.put((id)t1.get('OwnerId'),(Integer)t1.get('tsk1'));
                    ChkMapKey1 = string.valueof(TaskMap1.keyset());
            }
        }
        TaskSize1= TaskMap1.size();
        
        List<AggregateResult>EmailList1 = [SELECT Count(Id)eml, CreatedById FROM EmailMessage WHERE  CreatedById =: ids GROUP BY CreatedById];
        EmailSize1 = 0;
        if(EmailList1.size() > 0){
            for(AggregateResult e1 : EmailList1){
                 Integer EmailList = EmailMap1.get((id)e1.get('CreatedById'));
                 if(EmailList == null)
                    EmailMap1.put((id)e1.get('CreatedById'),(Integer)e1.get('eml'));
                    EmlMapKey1 = string.valueof(EmailMap1.keyset());
            }
        }
        EmailSize1= EmailMap1.size();
        
        List<AggregateResult>EmailList2 = [SELECT Count(Id)eml1, CreatedById FROM EmailMessage WHERE  CreatedById =: ids AND IsOpened = true GROUP BY CreatedById];
        EmailSize2 = 0;
        if(EmailList2.size() > 0){
            for(AggregateResult e2 : EmailList2){
                 Decimal EmailListdata2 = EmailMap2.get((id)e2.get('CreatedById'));
                 if(EmailListdata2 == null)
                    EmailMap2.put((id)e2.get('CreatedById'),(Integer)e2.get('eml1'));
                    EmlMapKey2 = string.valueof(EmailMap2.keyset());
            }
        }
        EmailSize2= EmailMap2.size();
        Integer currentMonth = system.today().month();
        
        List<AggregateResult>upsell1 = [SELECT OwnerId,Sum(Total_Toll_Free_Numbers__c)up1,Sum(Total_Toll_Free_Numbers__c)up2,Sum(Extra_Requirement_Charges__c)up3,
                                              Sum(Client_Portal_Locations__c)up4,Sum(Client_Portal_Premier_Locations__c)up5,Sum(Total_Users__c)up6 
                                        FROM Opportunity
                                        WHERE CALENDAR_MONTH(CreatedDate) =: currentMonth AND OwnerId =: ids GROUP BY OwnerId];
        UpsellMapSize1 = 0;
        UpsellMapSize2 = 0;
        UpsellMapSize3 = 0;
        UpsellMapSize4 = 0;
        UpsellMapSize5 = 0;
        UpsellMapSize6 = 0;
        if(upsell1.size()>0){
            for(AggregateResult up: upsell1){
                Decimal upsellData1 = UpsellMap1.get((id)up.get('OwnerId'));
                if(upsellData1 == null)
                    UpsellMap1.put((id)up.get('OwnerId'),(Decimal)up.get('up1'));
                    UpsellMapKey1 = string.valueof(UpsellMap1.keyset());
                    
                }
                for(AggregateResult up: upsell1){
                Decimal upsellData2 = UpsellMap2.get((id)up.get('OwnerId'));
                if(upsellData2 == null)
                    UpsellMap2.put((id)up.get('OwnerId'),(Decimal)up.get('up2'));
                    UpsellMapKey2 = string.valueof(UpsellMap2.keyset());
                }
                for(AggregateResult up: upsell1){
                Decimal upsellData3 = UpsellMap3.get((id)up.get('OwnerId'));
                if(upsellData3 == null)
                    UpsellMap3.put((id)up.get('OwnerId'),(Decimal)up.get('up3'));
                    UpsellMapKey3 = string.valueof(UpsellMap3.keyset());
                }
                for(AggregateResult up: upsell1){
                Decimal upsellData4 = UpsellMap4.get((id)up.get('OwnerId'));
                if(upsellData4 == null)
                    UpsellMap4.put((id)up.get('OwnerId'),(Decimal)up.get('up4'));
                    UpsellMapKey4 = string.valueof(UpsellMap4.keyset());
                }
                for(AggregateResult up: upsell1){
                Decimal upsellData5 = UpsellMap5.get((id)up.get('OwnerId'));
                if(upsellData5 == null)
                    UpsellMap5.put((id)up.get('OwnerId'),(Decimal)up.get('up5'));
                    UpsellMapKey5 = string.valueof(UpsellMap5.keyset());
                }
                for(AggregateResult up: upsell1){
                Decimal upsellData6 = UpsellMap6.get((id)up.get('OwnerId'));
                if(upsellData6 == null)
                    UpsellMap6.put((id)up.get('OwnerId'),(Decimal)up.get('up6'));
                    UpsellMapKey6 = string.valueof(UpsellMap6.keyset());
                }
                UpsellMapSize1 = UpsellMap1.size();
                UpsellMapSize2 = UpsellMap2.size();
                UpsellMapSize3 = UpsellMap3.size();
                UpsellMapSize4 = UpsellMap4.size();
                UpsellMapSize5 = UpsellMap5.size();
                UpsellMapSize6 = UpsellMap6.size();
                // oppMapSize6 = OppMap6.size();
        
        }
        
        
        List <Report> reportList = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Active_opportunity_Report'];
         reportId = (String)reportList.get(0).get('Id');
         List <Report> reportList1 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Active_Lead_Report'];
         reportId1 = (String)reportList1.get(0).get('Id');
         List <Report> reportList2 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Daily_Activities_All_Agent'];
         reportId2 = (String)reportList2.get(0).get('Id');
       
   }
   public static void test(){
        Integer a,b,c;
        a=1;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
        b=a;
        c=b;
        a=c;
    }
//   public class UserClass{
//       public String Name ='';
//       public String Id;
//       public List<leaddata> conversionSDRList;
//       public List<oppData> oppDataList = new List<oppData>();
//   }
//   public class leaddata {
//       public Id OwnerId;
//       public Integer ldCount = 0;
//       public Integer ConvertedCount = 0;
//   }
//   public class oppData {
//       public Integer oppcount = 0;
//       public Decimal ActoppAmount = 0.00;
//       public Decimal AmtClosed = 0.00;
//       public Decimal Amtproject = 0.00;
//       public Integer salesnum = 0;
//   }
 
}