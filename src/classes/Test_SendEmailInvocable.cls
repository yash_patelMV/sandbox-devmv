@isTest
public class Test_SendEmailInvocable {
    
    private static testMethod void test() {
        
        List<Case> CaseList = new List<Case>();
        
        Account acc = new Account(name='test');
        insert acc;
        
        Contact ct = new Contact(FirstName = 'test', LastName='data', Email='test@test.com');
        insert ct;
        
        Opportunity opp = new Opportunity(Name = 'test', StageName = 'Prospecting', CloseDate = System.today(), AccountId = acc.Id); 
        insert opp;
        
        Case c = new Case();
        c.ContactId = ct.Id;
        c.AccountId = acc.Id;
        c.Type = 'Billing Inquery';
        c.Repeat_Issue__c = true;
        c.Opportunity__c = opp.Id;
        c.Send_Email_with_Status_Change__c = true;
        insert c;
        
        CaseList.add(c);
        
        Test.startTest();
        SendEmailInvocable.SendEmailToCase(CaseList);
        Test.stopTest();
    }
}