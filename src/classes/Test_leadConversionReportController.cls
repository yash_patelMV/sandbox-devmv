@isTest
public class Test_leadConversionReportController {
    @isTest(SeeAllData='true')
	public static void test() {
	 test.startTest();
        Lead lead = new Lead();
        lead.FirstName='Trigger1';
        lead.LastName='Test1';
        lead.Company='Trigger Test1';
        lead.LeadSource='GetTimely Online';
        // lead.LastActivityDate = System.today()-2;
        // lead.Handicap__c=12;
        // lead.PostCode__c='1234';
        insert lead;
        
        System.debug('Created and inserted lead');
        
         Database.LeadConvert lc = new database.LeadConvert();
     lc.setLeadId(lead.Id);

     LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
     lc.setConvertedStatus(convertStatus.MasterLabel);
     Database.LeadConvertResult lcr = Database.convertLead(lc);
     
     Opportunity opp = new Opportunity();
     opp.Name= 'Test';
     opp.StageName= 'Closed Won';
     opp.CloseDate=System.today();
     opp.Total_Toll_Free_Numbers__c = 12;
     insert opp;
     
     Opportunity opp1 = new Opportunity();
     opp1.Name= 'Test';
     opp1.StageName= 'Project-Closed Won';
     opp1.CloseDate=System.today();
     insert opp1;
     
     Opportunity opp2 = new Opportunity();
     opp2.Name= 'Test';
     opp2.StageName= 'Needs Analysis';
     opp2.CloseDate=System.today();
     insert opp2;
     
     Task t = new Task();
     t.Subject = 'Test';
     t.ActivityDate = System.today();
     insert t;
     
     List <Report> reportList = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Active_opportunity_Report' LIMIT 1];
        // String reportId = (String)reportList.get(0).get('Id');
         List <Report> reportList1 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Active_Lead_Report' LIMIT 1];
        // String reportId1 = (String)reportList1.get(0).get('Id');
         List <Report> reportList2 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Daily_Activities_All_Agent' LIMIT 1];
        // String reportId2 = (String)reportList2.get(0).get('Id');
     
    //  Event e = new Event();
    //  e.Subject = 'Test';
    //  e.ActivityDate = System.today();
    //  insert e;
     
     leadConversionReportController lcrc = new leadConversionReportController();
     leadConversionReportController.test();
	}

}