@isTest
public class Test_Case_FormController {
	
    public static testmethod void getCase(){
        Account acc = new Account(name='test');
        insert acc;
        
        Contact ct = new Contact(FirstName = 'test', LastName='data', Email='test@test.com');
        insert ct;
		
		Opportunity opp = new Opportunity(Name = 'test', StageName = 'Prospecting', CloseDate = System.today(), AccountId = acc.Id); 
        insert opp;
        
        Case c = new Case();
        c.ContactId = ct.Id;
        c.AccountId = acc.Id;
        c.Type = 'Billing Inquery';
        c.Repeat_Issue__c = true;
        c.Opportunity__c = opp.Id;
 	
        test.startTest();
        
        insert c;
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        Case_FormController cf = new Case_FormController(sc);
        cf.EmailId = 'test1@test.com';
        cf.saveRecord();
        cf.FirstName = 'testing';
    	cf.LastName = 'data';
    	cf.AccountName = 'test';
    	cf.AccountNumber = 'Opp1';
    	cf.PhoneNumber = '873487783';
    	cf.PrefferdMehtod = 'Phone';
    	cf.InqueryReason = 'Billing Inquiry';
    	cf.Urgency = 'Immediately same day';
    	cf.Description = 'testing';
        cf.Submittedby = 'testing';
    	cf.isChecked = true;
       	cf.saveRecord();
		test.stopTest();
    }
}