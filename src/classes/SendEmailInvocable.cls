public with sharing class SendEmailInvocable {

    @InvocableMethod(label='Send Case Email After 14 days' description='Send Email to Case after 15 days and save as activity also for them' category= 'Case')
    public static void SendEmailToCase(List<Case> CasesList) {

        List<Messaging.SingleEmailMessage> EmailMessagingList = new List<Messaging.SingleEmailMessage>();
        
        Group ManagerQueue = [SELECT Id, DeveloperName from Group where Type = 'Queue' AND DeveloperName = 'Manager_Queue'];
        
        Map<String,Id> EmailTemplateMap = new Map<String, Id>();
        
        List<String> templateList = new List<String>{'Case_Send_Email_After_2_Weeks_Case_Closed'};
        
        for(EmailTemplate et : [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName IN: templateList]){
            EmailTemplateMap.put(et.DeveloperName, et.Id);
        }
        
        System.debug('Cases List is as'+CasesList);
        
        for(Case c : CasesList){
     
            String emailTemplate = 'Case_Send_Email_After_2_Weeks_Case_Closed';
        
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setTargetObjectId(c.ContactId);
            message.setWhatId(c.Id);
            message.setTemplateId(EmailTemplateMap.get(emailTemplate));
            message.setSaveAsActivity(true);
            
            EmailMessagingList.add(message);
        }
        
        System.debug('EmailMessagingList.size('+EmailMessagingList.size());
        
        try{
            if(EmailMessagingList.size() > 0){
                Messaging.sendEmail(EmailMessagingList);
            }
        }catch(Exception e){
            System.debug('Email error' +e.getMessage());
        }
    }
}