@isTest(SeeAllData='true')
public class Test_dashboardReportCtrl {
   @isTest
    public static void test() {
        List<String> UserNameList = new List<String>();
        UserNameList.add('Aleen Schiavo');
        UserNameList.add('Elizabeth Healy');
        // UserNameList.add('Jamie Portella');
        UserNameList.add('Tim Clarke');
        UserNameList.add('Steve Garnham');
        UserNameList.add('Ricky Morse');
        
        List<String> NotIncludedStatuses = new List<String>();
        NotIncludedStatuses.add('Dead - Bad Data-number_email');
        NotIncludedStatuses.add('Dead-DRTV');
        NotIncludedStatuses.add('Dead-Looking for a job');
        NotIncludedStatuses.add('Dead - Outbound To Small');
        NotIncludedStatuses.add('Dead- They are a call center');
        NotIncludedStatuses.add('Dead - They are not looking for service');
        NotIncludedStatuses.add('Operations');
        NotIncludedStatuses.add('Spam');
        
        List<String> NotIncludedSource = new List<String>();
        NotIncludedSource.add('Uplead');
        NotIncludedSource.add('Cold Calling');
        NotIncludedSource.add('');
        
        List<String>ids = new List<String>();
        List<User> usr = [Select Id from User Where Name In: UserNameList];
        for(User us: usr){
            ids.add(us.id);
        }
        List<Lead>ldList = new List<Lead>();
	   
		for(Integer i=0;i<4;i++){
        	Lead ld = new Lead();
			ld.LastName = 'Test'+i;
			ld.Company = 'TestCompany'+i;
			ld.AnnualRevenue = 5000+i;
			ld.OwnerId = usr[i].Id;
			ld.LeadSource = 'Cold Calling';
			ld.Status = 'Open';
			ldList.add(ld);    
			System.debug(ld);
        }
		insert ldList; 
		
        List<Lead> ldList1 = new List<Lead>();
        
        for(Integer i=0;i<5;i++){
      		 Lead ld = new Lead();
			ld.LastName = 'Test'+i;
			ld.Company = 'TestCompany'+i;
			ld.AnnualRevenue = 5000+i;
			ld.OwnerId = usr[i].Id;
			ld.LeadSource = '360Connect';
			ld.Isconverted=true;
			ld.Status = 'Open';
			ldList.add(ld);    
			System.debug(ld);
        }
        insert ldList1; 
        
        Lead ld = new Lead();
			ld.LastName = 'Test';
			ld.Company = 'TestCompany';
			ld.AnnualRevenue = 50014;
			ld.OwnerId = usr[0].Id;
			ld.LeadSource = 'Uplead';
			ld.Status = 'Open';
         insert ld;  
        List<Opportunity> oppList = new List<Opportunity>();
        for(Integer i = 0; i<5;i++){
            Opportunity opp = new Opportunity();
            opp.StageName = 'Closed Won';
            opp.Name = 'test'+i;
            opp.Amount = 1000;
            opp.CloseDate = date.valueOf('2020-07-09');
            opp.Total_Toll_Free_Numbers__c = 1;
            opp.Local_TFN_Amount__c = 1;
            opp.OwnerId = usr[i].Id;
            opp.Extra_Requirements__c = '';
            opp.Extra_Requirement_Charges__c = 1;
            oppList.add(opp);
    	}
        insert oppList;
        
        Opportunity opp1 = new Opportunity();
        opp1.StageName = 'Needs Analysis';
        opp1.Name = 'test';
        opp1.Amount = 1000;
        opp1.CloseDate = date.valueOf('2020-07-09');
        opp1.Total_Toll_Free_Numbers__c = 1;
        opp1.Local_TFN_Amount__c = 1;
        //opp1.OwnerId = usr[0].Id;
        opp1.Extra_Requirements__c = '1';
        opp1.Extra_Requirement_Charges__c = 1;
        opp1.LeadSource = '360Connect';
        insert opp1;
        
        Task tsk = new Task();
        tsk.Type = 'Call';
        tsk.WhoId = ldList[1].Id;
        tsk.ActivityDate = System.today();
        tsk.OwnerId = usr[0].Id;
        
		insert tsk;
		
		Task tsk1 = new Task();
        tsk1.Type = 'Email';
        tsk1.WhoId = ldList[1].Id;
        tsk1.ActivityDate = System.today();
        tsk1.OwnerId = usr[1].Id;
        tsk1.Subject = 'Sent';
		insert tsk1;
		
		Task tsk2 = new Task();
        tsk2.Type = 'Email';
        tsk2.WhoId = ldList[2].Id;
        tsk2.ActivityDate = System.today();
        tsk2.OwnerId = usr[2].Id;
        tsk2.Subject = 'Opened';
		insert tsk2;
		
		String Name = UserInfo.getFirstName();
		dashboardReportCtrl.disable = false;
		dashboardReportCtrl.selectedUser= 'Rickey';
		Database.LeadConvert lc = new database.LeadConvert();
         lc.setLeadId(ld.Id);
    
         LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
         lc.setConvertedStatus(convertStatus.MasterLabel);
         Database.LeadConvertResult lcr = Database.convertLead(lc);
	
        List <Report> reportList = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Active_opportunity_Report' LIMIT 1];
        List <Report> reportList1 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Active_Lead_Report' LIMIT 1];
		List <Report> reportList2 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Daily_Activities_All_Agent' LIMIT 1];
		List <Report> reportList3 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Prospecting_Lead_Report'];
        List <Report> reportList4 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Prospecting_Opportunity_Report'];
        List <Report> reportList5 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Prospecting_All_Lead_Report'];
        List <Report> reportList6 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Active_All_Lead_Report'];
        List <Report> reportList7 = [SELECT Id,DeveloperName FROM Report WHERE DeveloperName ='Upsell_Opportunity_Report'];
        dashboardReportCtrl.StartDate =System.today()-1;
        dashboardReportCtrl.EndDate =System.today()+1;
        dashboardReportCtrl.OwnerIdList=ids;
		
		dashboardReportCtrl.LeadConversionReport();
        

		Date todayDate = Date.today();
		Integer currentMonth = todayDate.month();
		Integer currentYear = todayDate.year();

		
		dashboardReportCtrl.changeVar();
		dashboardReportCtrl.selectedTask = 'BPO';
		
	}
	
	@isTest
	public static void Filtersmethods(){
		Test.startTest();
			dashboardReportCtrl.Quarterdata = 1;
			
			dashboardReportCtrl dc = new dashboardReportCtrl();
            
            dc.exportData1();
            dc.exportData();
            dc.exportDataWarning();
            dc.exportDataProspecting();
            dc.exportDataUpsell();
        	dc.exportDataRickyMonthly();
        	dc.exportDataGaryMonthly();
       		dc.OnchangeQuarter();
        	dc.yearData();
        	dc.MonthlyData();
			//dc.filter();
			dc.changeUser();
			
        Test.stopTest();
	}
	
    @isTest
    public static void Test2(){
        Test.startTest();
			dashboardReportCtrl.Quarterdata = 2;
        	dashboardReportCtrl.selectedMonth = 2;
			
			dashboardReportCtrl dc = new dashboardReportCtrl();
            
       		dc.OnchangeQuarter();
        	dc.yearData();
        	dc.MonthlyData();
			
			
        Test.stopTest();
    }
    
	@isTest
	public static void testProspectingReport(){
		Test_dashboardReportCtrl.test();
		Test.startTest();
			dashboardReportCtrl.ProspectingReport();
		Test.stopTest();
	}
        
}