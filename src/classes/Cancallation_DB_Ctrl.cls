public with sharing class Cancallation_DB_Ctrl {

    public static List<Report> databaseReportQuery(){
        List<Report> ReportList = [SELECT Id, DeveloperName FROM Report];
        return ReportList;
    }

    public static List<Opportunity> databaseOptyQuery(String reportName, Cancellation_Report_Ctrl.filterDateWpr DateFilter){
        
        List<Opportunity> resultOptyList = new List<Opportunity>();
        Map<string,List<String>> filters = new Map<String,List<String>>();
        List<Id> userListFilter = new List<Id>();
        List<string> wonFilter = new List<String>();
        List<String> reasonListFilter = new List<String>();
        List<String> SiteListFilter = new List<String>();
        List<String> LeadSourceListFilter = new List<String>();     
        List<String> ProjectLiveDate = new List<String>();    

        filters = fetchFilters(reportName);
        
        String closedWon = 'Closed Won';
        Date ReportStartDate = DateFilter.StartDate;
        Date ReportEndDate = DateFilter.EndDate;
        
        String query = 'SELECT Id,Name,OwnerId,Owner.Name,Cancellation_Reason__c,StageName,Site__c,Set_Up_Fees__c,Amount,NextStep,Cancellation_Date__c,CloseDate,LeadSource from Opportunity Where Cancellation_Date__c >=: ReportStartDate AND Cancellation_Date__c <=: ReportEndDate '; //AND CloseDate >=: ReportStartDate AND CloseDate <=: ReportEndDate
       
        //User/OwnerId Filters
        if(filters.containsKey('User Filter')){
            userListFilter = DBUserQuery(filters.get('User Filter')).values();
            query += ' AND OwnerId NOT IN :userListFilter';
        }

        //Cancellation Reason Filter
        if(filters.containsKey('Reason Filter')){
            reasonListFilter = filters.get('Reason Filter');
            query += ' AND Cancellation_Reason__c IN :reasonListFilter';
        }

        //Site Filters
        if(filters.containsKey('Site Filter')){
            SiteListFilter = filters.get('Site Filter');
            query += ' AND Site__c IN :SiteListFilter';
        }

        //Lead Source Filters
        if(filters.containsKey('Lead Source Filter')){
            LeadSourceListFilter = filters.get('Lead Source Filter');
            query += ' AND LeadSource IN :LeadSourceListFilter';
        }
        
         //Close Won Filters.
        if(filters.containsKey('Won Filter')){
             wonFilter = filters.get('Won Filter');
             system.debug('Won Filters' +wonFilter);    
             query += ' AND StageName IN: wonFilter';
         }

         if(filters.containsKey('Projected Account Go Date Greater than')){
             ProjectLiveDate = filters.get('Projected Account Go Date Greater than');             
             Date pDate = Date.valueOf((ProjectLiveDate+'').removeEnd(')').removeStart('('));
             system.debug('Project Live Date Filters'+ pDate);    
             query += ' AND Projected_Account_Go_Live_Date__c >: pDate';
         }
        System.debug(query);

        resultOptyList = Database.query(query);
        return resultOptyList;
    }

    public static List<Opportunity> databaseCloseWonQuery(String reportName, Cancellation_Report_Ctrl.filterDateWpr DateFilter){
        
        Date ReportStartDate = DateFilter.StartDate;
        Date ReportEndDate = DateFilter.EndDate;
        
        List<Opportunity> optyList = new List<Opportunity>();
        String query = 'SELECT Id,Name,OwnerId,Cancellation_Reason__c,StageName,Site__c,Owner.Name,Set_Up_Fees__c,Amount,NextStep,Cancellation_Date__c,CloseDate,LeadSource,Projected_Account_Go_Live_Date__c from Opportunity Where CloseDate >=: ReportStartDate AND CloseDate <=: ReportEndDate '; //AND Cancellation_Date__c >=: ReportStartDate AND Cancellation_Date__c <=: ReportEndDate
        Map<String,List<String>> filterMap = new Map<String,List<String>>();
        filterMap = fetchFilters(reportName);
        List<string> wonFilter = new List<String>();
        List<Id> userListFilter = new List<Id>();
        List<String> lsFilter = new List<String>();
        List<String> siteFilters = new List<String>();       
 
        //User/OwnerId Filters
        if(filterMap.containsKey('User Filter')){
            userListFilter = DBUserQuery(filterMap.get('User Filter')).values();
             query += ' AND OwnerId NOT IN :userListFilter';
         }

         //Lead Source filters
        if(filterMap.containsKey('Lead Source Filter')){
            lsFilter = filterMap.get('Lead Source Filter');
            query += ' AND LeadSource IN: lsFilter';
        }

        //Site source filters.
        if(filterMap.containsKey('Site Filter')){
            siteFilters = filterMap.get('Site Filter');
            query += ' AND Site__c IN: siteFilters';
        }

        //Close Won Filters.
        if(filterMap.containsKey('Won Filter')){
             wonFilter = filterMap.get('Won Filter');
             system.debug('Won Filters' +wonFilter);    
             query += ' AND StageName IN: wonFilter';
         }
        optyList = Database.query(query);
        return optyList;
    }

    public static Map<String,Id> DBUserQuery(List<String> userName){
        Map<String,Id> userMap = new Map<String,Id>();
        for(User un : [select Id,Name from User Where Name IN: userName]){
            userMap.put(un.Name,un.Id);
        }
        return userMap;
    }

    public static Map<String,List<String>> fetchFilters(String reportName){
        Map<String,List<String>> resultFilterMap = new Map<String,List<String>>();

        for(Cancellation_Report_Config__c crc : [select Id, Name, Start_Date_greater_than__c, Lead_Source_Filter__c, Reasons_Filter__c, Site_Filter__c, User__c, Won_Condition__c, Next_Step_Contain__c, Close_Date_greater_equal_than__c, Close_Date_less_than__c, Projected_Account_Go_Date_Greater_than__c from Cancellation_Report_Config__c where Name =: reportName]){
            if(crc.Reasons_Filter__c != null){
                resultFilterMap.put('Reason Filter',crc.Reasons_Filter__c.split(';'));
            }
            if(crc.Lead_Source_Filter__c != null){
                resultFilterMap.put('Lead Source Filter',crc.Lead_Source_Filter__c.split(';'));
            }
            if(crc.Site_Filter__c != null){
                resultFilterMap.put('Site Filter',crc.Site_Filter__c.split(';'));
            }
            if(crc.User__c != null){
                resultFilterMap.put('User Filter',crc.User__c.split(';'));
            }
            if(crc.Won_Condition__c != null){
                resultFilterMap.put('Won Filter',crc.Won_Condition__c.split(';'));
            }
            if(crc.Next_Step_Contain__c != null){
                resultFilterMap.put('Next Step not Contain',new List<String>{String.valueOf(crc.Next_Step_Contain__c)});
            }
            if(crc.Start_Date_greater_than__c != null){
                resultFilterMap.put('Start Date greater than',new List<String>{String.valueOf(crc.Start_Date_greater_than__c)});
            }
            
            if(crc.Close_Date_greater_equal_than__c != null){
                resultFilterMap.put('Close Date greater equal than',new List<String>{String.valueOf(crc.Close_Date_greater_equal_than__c)});
            }
            
            if(crc.Close_Date_less_than__c != null){
                resultFilterMap.put('Close Date less than',new List<String>{String.valueOf(crc.Close_Date_less_than__c)});
            }

            if(crc.Projected_Account_Go_Date_Greater_than__c != null){
                resultFilterMap.put('Projected Account Go Date Greater than',new List<String>{String.valueOf(crc.Projected_Account_Go_Date_Greater_than__c)});
            }
        }
        return resultFilterMap;
    }
}