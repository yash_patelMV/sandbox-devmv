public class AEManagementController {
    public List<Opportunity> oppList {get;set;}
    public Map<Id,User> usrMap {get;set;}
    public List<User> usrList {get;set;}
    public List<AEWrapper> wrap {get;set;}
    public String selectedUsr {get;set;}
    public String selecteduserId {get;set;}
    public String searchOpp {get;set;}
    public Integer updatedSize {get;set;}
    public String selectedType{get;set;}
    public Boolean flag {get;set;}
    
    public AEManagementController(){
        selecteduserId = null;
        wrap = new List<AEWrapper>();
        updatedSize = 0;
        //usrMap = [Select Id,Name From User Where IsActive = true AND ProfileId != null Order By Name ASC];
        usrMap = new Map<Id,User>([ Select Id,Name From User Where IsActive = true AND ProfileId != null Order By Name ASC ]);
        usrList = usrMap.values();
        flag = true;
        getData();
    }
    
    public PageReference getData(){
        // if(!String.isBlank(selecteduserId)){
            wrap = new List<AEWrapper>();
            selectedType = 'All';
        	searchOpp = '';
            oppList = [Select Id,Name,Account_Executive__c,Account.Name,Account_Executive__r.Name From Opportunity Where Account_Executive__c =: selecteduserId Order By Name Limit 1000];
            for(Opportunity opp : oppList){
                AEWrapper wrapObj = new AEWrapper();
                wrapObj.checked = false;
                wrapObj.oppId = opp.Id;
                wrapObj.Name = opp.Name;
                wrapObj.AccountName = opp.Account.Name;
                wrapObj.AE = opp.Account_Executive__c;
                wrapObj.AEName = opp.Account_Executive__r.Name;
                wrap.add(wrapObj);
            }
        // }else{
        //     wrap = new List<AEWrapper>();
        // }
        if(flag == true)    updatedSize = 0;
        return null;
    }
    
    public PageReference searchOpportunity(){
        // if(!String.isBlank(selecteduserId)){
            wrap = new List<AEWrapper>();
            String s = '%'+searchOpp+'%';
            System.debug(selectedType);
            String query = 'Select Id,Name,Account_Executive__c,Account.Name,Account_Executive__r.Name, Type From Opportunity Where Name LIKE: s AND Account_Executive__c =: selecteduserId';
            if(selectedType == null)    selectedType = '';
            if(selectedType != 'All'){
                query += ' AND Type =: selectedType';
            }
            query += ' Order By Name';
            // [Select Id,Name,Account_Executive__c,Account.Name,Account_Executive__r.Name, Type From Opportunity Where Account_Executive__c =: selecteduserId AND Name LIKE: s AND Type =: selectedType Order By Name]
            oppList = Database.query(query);
            for(Opportunity opp : oppList){
                AEWrapper wrapObj = new AEWrapper();
                wrapObj.checked = false;
                wrapObj.oppId = opp.Id;
                wrapObj.Name = opp.Name;
                wrapObj.AccountName = opp.Account.Name;
                wrapObj.AE = opp.Account_Executive__c;
                wrapObj.AEName = opp.Account_Executive__r.Name;
                wrap.add(wrapObj);
            }
        // }else{
        //     wrap = new List<AEWrapper>();
        // }
        updatedSize = 0;
        return null;
    }
    
    public List<SelectOption> getListOfUser(){
        List<User> Users = [select Id,Username,name From User Order By Name ASC];
        List<SelectOption> UserOptionList1 = new List<SelectOption>();
        UserOptionList1.add(new SelectOption( '' ,'Unassigned'));
        for(User u : Users ){
            UserOptionList1.add(new SelectOption(u.Id , u.Name));
        }
        return UserOptionList1;
    }
    
    public List<SelectOption> getTypePicklistValue(){
        List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(new List<String>{'Opportunity'});
        List<SelectOption> values = new List<SelectOption>();
        for(Schema.DescribeSobjectResult res : results) {
            values.add(new SelectOption('All', 'All Type'));
            for (Schema.PicklistEntry entry : res.fields.getMap().get('type').getDescribe().getPicklistValues()) {
                if (entry.isActive()) {
                    values.add(new SelectOption(entry.getValue(), entry.getLabel()));
                }
            }
            values.add(new SelectOption('', 'Blank Type'));
        }
        System.debug(values);
        return values;
    }
    
    public PageReference selectAll(){
        for(AEWrapper w : wrap){
            if(w.checked == false){
                w.checked = true;
            }else{
                w.checked = false;
            }
        }
        return null;
    }
    
    public PageReference addExecutive(){
        List<Opportunity> opList = new List<Opportunity>();
        for(AEWrapper w : wrap){
            if(w.checked == true){
                Opportunity op = new Opportunity();
                op.Id = w.oppId;
                op.Account_Executive__c = selectedUsr;
                opList.add(op);
            }
        }
        System.debug(opList);
        update opList;
        updatedSize = opList.size();
        flag = false;
        getData();
        return null;
    }
    
    public PageReference unAssignExec(){
        List<Opportunity> opList = new List<Opportunity>();
        selectedUsr = '';
        for(AEWrapper w : wrap){
            if(w.checked == true){
                Opportunity op = new Opportunity();
                op.Id = w.oppId;
                op.Account_Executive__c = null;
                opList.add(op);
            }
        }
        update opList;
        updatedSize = opList.size();
        flag = false;
        getData();
        return null;
    }
    
    public class AEWrapper{
        public Boolean checked {get;set;}
        public String oppId {get;set;}
        public String Name {get;set;}
        public String AE {get;set;}
        public String AccountName {get;set;}
        public String AEName {get;set;}
    }
}