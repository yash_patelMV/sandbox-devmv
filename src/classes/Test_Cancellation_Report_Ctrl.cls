@isTest
public with sharing class Test_Cancellation_Report_Ctrl {
  @isTest(SeeAllData = true)
  public static void test(){
      
      Cancellation_Report_Config__c con = new Cancellation_Report_Config__c();
      con.Reasons_Filter__c = 'Billing Issues';
      con.User__c = 'Aleen Schiavo';
      con.Operations_Reason__c = 'Billing Issues; Agent Errors; Communication Issues';
      con.Billing_Money_Reason__c  = 'Billing Issues; Agent Errors; Communication Issues';
      con.Other_Reasons__c  = 'Billing Issues; Agent Errors; Communication Issues';
      con.Covid_Reason__c  = 'Billing Issues; Agent Errors; Communication Issues';
      con.Name = 'Cancellation Report';
      con.Close_Date_greater_equal_than__c = system.today();
      insert con;
      
      
      List<User>usList = [SELECT ID,Name FROM User WHERE IsActive = true AND Profile.UserLicense.Name = 'Salesforce' ];
      
      List<Opportunity> OppList = new List<Opportunity>();
      
      for(Integer i=0; i<100; i++){
          Opportunity opp= new Opportunity();
          opp.Name = 'Test';
          opp.CloseDate = i < 50 ? System.today().addYears(-1) : System.today();
          opp.StageName = 'Closed Won';
          opp.OwnerId =  usList[1].Id;
          opp.Cancellation_Reason__c = 'Billing Issues';
          opp.Site__c = 'Santa Ana CA';
          opp.LeadSource = '360Connect';
          opp.Cancellation_Date__c = System.today()+3;
          opp.Amount = 52;
          opp.Set_Up_Fees__c = 454.00;
          OppList.add(opp);
      }
      
      
       Test.startTest();
      insert OppList;
      
     
      Cancellation_Report_Ctrl crc = new Cancellation_Report_Ctrl();
 
      crc.salesVSDisconnectReport();
      crc.repsalesReveVSMonthlyGoal();
      crc.thirdTable();
      crc.leadSourceDisc();
      crc.cancelReason();
      crc.Monthlyfilter();
      crc.QuarterFilter();
      crc.YearFilter();
      crc.tabFilter();
      crc.demo();
      Test.stopTest();

  }
}