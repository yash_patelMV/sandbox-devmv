public with sharing class DashboardReportCtrlQuery {

    public static Boolean flag = true;
    public static Map<String, String> conditionsMap {get;set;}
    public static String CompanyName ='[not provided]';
  
    
    public DashboardReportCtrlQuery(){
        getConditionData();
    }

    public static void getConditionData(){
        List<Condition__c> conditionList = [ SELECT Id, Name,Field_Name__c, Condition__c, Operation__c, Field_Name__r.Name, Field_Name__r.Report_Name__c, Field_Name__r.Report_Name__r.Name  FROM Condition__c];
       
        conditionsMap = New Map<String, String>();
        for(Condition__c c : conditionList){

            if(!conditionsMap.containsKey(c.Field_Name__r.Name)){
                String ConditionString = '';
                
                if(c.Operation__c != 'NOT LIKE' && c.Operation__c != 'LIKE'){
                    ConditionString = ' AND '+ c.Name +' '+ c.Operation__c + ' ( ' + c.Condition__c + ' ) ';
                }else if(c.Operation__c == 'NOT LIKE'){
                    ConditionString = ' AND ( NOT '+ c.Name +' LIKE  ' + c.Condition__c + ' ) ';
                }else if(c.Operation__c == 'LIKE'){
                    ConditionString = ' AND ( '+c.Name +' '+ c.Operation__c + '  ' + c.Condition__c + ' )';
                }
                
                
                conditionsMap.put(c.Field_Name__r.Name, ConditionString);
            }else{
                String conditionString = conditionsMap.get(c.Field_Name__r.Name);
                if(c.Operation__c != 'NOT LIKE' && c.Operation__c != 'LIKE'){
                    ConditionString += ' AND '+ c.Name +' '+ c.Operation__c + ' ( ' + c.Condition__c + ' ) ';
                }else if(c.Operation__c == 'NOT LIKE'){
                    ConditionString += ' AND ( NOT '+ c.Name +' LIKE  ' + c.Condition__c + ' ) ';
                }else if(c.Operation__c == 'LIKE'){
                    if(ConditionString.contains('LIKE') && !ConditionString.contains('NOT')){
                        System.debug('HI there');
                        ConditionString = ConditionString.removeEnd(')');
                        ConditionString += ' OR '+ c.Name +' '+ c.Operation__c + '  ' + c.Condition__c + ')  ';
                    }else{
                        ConditionString += ' AND '+ c.Name +' '+ c.Operation__c + '  ' + c.Condition__c + '  ';
                    }
                }
                
                conditionsMap.put(c.Field_Name__r.Name, ConditionString);
            }
        }
    }


    public static List<AggregateResult> getAggregateList(String Team, String ReportType, List<String> OwnerIdList , Integer Days,Date StartDate,Date EndDate){
        if(flag){
            getConditionData();
            flag = false;
        }
        List<AggregateResult> arList = new List<AggregateResult>();
        // With Map
        // if(ReportType == 'Monthly Lead Count TAS') arList = getAggregateData( StartDate,EndDate,OwnerIdList,' Count(Id)id, OwnerId ',' Lead ',StartDate == null ?' OwnerId IN: OwnerIdList '+conditionsMap.get(ReportType)+' AND CreatedDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList AND  Status  IN: LeadStatusInTAS AND LeadSource  IN: LeadSourceInTAS  AND  DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ');

        // Lead Conversion Report
        if(ReportType == 'Monthly Lead Count TAS') arList = getAggregateData( StartDate,EndDate,OwnerIdList,' Count(Id)id, OwnerId ',' Lead ',StartDate == null ?' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count TAS')+' AND CreatedDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count TAS')+'  AND  DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Monthly Lead Count BPO') arList = getAggregateData( StartDate,EndDate,OwnerIdList,' Count(Id)id, OwnerId ',' Lead ',StartDate == null ?' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count BPO')+' AND IsConverted = false AND CreatedDate = THIS_MONTH GROUP BY OwnerId  ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count BPO')+' AND IsConverted = false AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Active Opp Total')arList = getAggregateData(StartDate,EndDate,OwnerIdList,Team == 'TAS'?' SUM(Amount)amt, OwnerId, Count(Id)id ': ' SUM(Monthly_Revenue__c) amt, OwnerId, Count(Id)id',' Opportunity ', Team == 'TAS' ? ' OwnerId IN: OwnerIdList  '+conditionsMap.get('Active Opts Total')+'  GROUP BY OwnerId  ' :'OwnerId IN: OwnerIdList  '+conditionsMap.get('Active Opts Total BPO')+' GROUP BY OwnerId');
        if(ReportType == 'Active Opp Total Amount')arList = getAggregateData(StartDate,EndDate,OwnerIdList,Team == 'TAS' ? ' SUM(Amount)amt, OwnerId ': ' SUM(Monthly_Revenue__c) amt, OwnerId ',' Opportunity ', Team == 'TAS' ? ' OwnerId IN: OwnerIdList  '+conditionsMap.get('Active Opts Total')+'  GROUP BY OwnerId  ' :' Monthly_Revenue__c != null AND OwnerId IN: OwnerIdList  '+conditionsMap.get('Active Opts Total BPO')+' GROUP BY OwnerId');
        if(ReportType == 'Daily Activity')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' Count(ID) id, OwnerId ' ,' Task ',' OwnerId IN: OwnerIdList '+conditionsMap.get('Daily Activity Count')+'   AND CreatedDate = TODAY GROUP BY OwnerId ');
        if(ReportType == 'Monthly Activity Count')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' Count(ID) id, OwnerId ' ,' Task ', StartDate == null ?' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Activity Count')+'   AND CreatedDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Activity Count')+'   AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId  ' );
        //if(ReportType == 'Monthly Activity Count BPO')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' Count(ID) recdCount, OwnerId ' ,' Task ', StartDate == null ?' OwnerId IN: OwnerIdList AND (NOT Subject LIKE: SubjectNotIN) AND  CreatedDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList AND (NOT Subject LIKE: SubjectNotIN) AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ' );
        if(ReportType == 'Monthly Activity')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' Count(ID) id, OwnerId ' ,' Task ',StartDate == null ?' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Activity')+' AND CreatedDate = THIS_MONTH GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Activity')+' AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId  ' );
        if(ReportType == 'Active Lead Count TAS')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Lead',StartDate == null ? ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count TAS')+' AND Company !=: CompanyName  AND  CreatedDate = THIS_MONTH GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count TAS')+' AND Company !=: CompanyName AND  DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ' ); 
        if(ReportType == 'Active Lead Count BPO')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Lead',StartDate == null ? ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count BPO')+'   AND  CreatedDate = THIS_MONTH  GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count BPO')+'  AND  DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ' ); 
        if(ReportType == 'Lead Converted TAS')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Lead',StartDate == null ? ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted TAS')+' AND  ConvertedDate = THIS_MONTH GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted TAS')+' AND  ConvertedDate >=: StartDate AND ConvertedDate <=: EndDate GROUP BY OwnerId ' ); 
        if(ReportType == 'Lead Converted BPO')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Lead',StartDate == null ? ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted BPO')+'  AND  ConvertedDate = THIS_MONTH GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted BPO')+'  AND  ConvertedDate >=: StartDate AND ConvertedDate <=: EndDate GROUP BY OwnerId ' ); 
        if(ReportType == 'Number of Sales')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Opportunity',StartDate == null ? ' OwnerId IN: OwnerIdList  '+conditionsMap.get('Number of Sales')+' AND CloseDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Number of Sales')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Amount CloseWon')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'SUM(Amount)amt, OwnerId','Opportunity',StartDate == null ? ' OwnerId IN: OwnerIdList '+conditionsMap.get('Amount Closed Won')+' AND CloseDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Amount Closed Won')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Amount CloseWon Project')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'SUM(Amount)amt, OwnerId','Opportunity',StartDate == null ?  ' OwnerId IN: OwnerIdList '+conditionsMap.get('Amount Closed Won Project')+' AND CloseDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Amount Closed Won Project')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        
        // USING MAP
        if(ReportType == 'Active Lead Count')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Lead',Team =='TAS' ? StartDate == null ? ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count TAS')+' AND Company !=: CompanyName  AND  CreatedDate = THIS_MONTH GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count TAS')+' AND Company !=: CompanyName AND  DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ' : StartDate == null ? ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count BPO')+'   AND  CreatedDate = THIS_MONTH  GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList '+conditionsMap.get('Active Lead Count BPO')+'  AND  DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ' ); 
        if(ReportType == 'Monthly Lead Count') arList = getAggregateData( StartDate,EndDate,OwnerIdList,' Count(Id)id, OwnerId ',' Lead ',Team == 'TAS' ? StartDate == null ?' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count TAS')+'  AND CreatedDate = THIS_MONTH GROUP BY OwnerId ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count TAS')+'  AND  DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ' : StartDate == null ?' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count BPO')+' AND IsConverted = false AND CreatedDate = THIS_MONTH GROUP BY OwnerId  ' : ' OwnerId IN: OwnerIdList '+conditionsMap.get('Monthly Lead Count BPO')+' AND IsConverted = false AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Lead Converted')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Lead',Team == 'TAS' ? StartDate == null ? ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted TAS')+' AND  ConvertedDate = THIS_MONTH GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted TAS')+' AND   ConvertedDate >=: StartDate AND ConvertedDate <=: EndDate GROUP BY OwnerId ' : StartDate == null ? ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted BPO')+'  AND  ConvertedDate = THIS_MONTH GROUP BY OwnerId ': ' OwnerId IN: OwnerIdList AND IsConverted = true '+conditionsMap.get('Lead Converted BPO')+'  AND  ConvertedDate >=: StartDate AND ConvertedDate <=: EndDate GROUP BY OwnerId ' ); 



        // Warning Not Touched
        if(ReportType == 'Warning Not Touched Opp')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId',' Opportunity ',  'OwnerId IN: OwnerIdList AND LastActivityDate < LAST_N_DAYS : '+Days+' '+conditionsMap.get('Opportunity TAS')+'  GROUP BY OwnerId ');
        if(ReportType == 'Opportunities not touched in 2 days')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId',' Opportunity ',  'OwnerId IN: OwnerIdList AND LastActivityDate < LAST_N_DAYS : 2 ' +conditionsMap.get('Opportunity TAS')+ '  GROUP BY OwnerId ');
        if(ReportType == 'Opportunities not touched in 5 days')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId',' Opportunity ',  'OwnerId IN: OwnerIdList AND LastActivityDate < LAST_N_DAYS : 5 ' +conditionsMap.get('Opportunity TAS')+ '  GROUP BY OwnerId ');
        if(ReportType == 'Opportunities not touched in 10 days')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId',' Opportunity ',  'OwnerId IN: OwnerIdList AND LastActivityDate < LAST_N_DAYS : 10 ' +conditionsMap.get('Opportunity TAS')+'  GROUP BY OwnerId ');
        if(ReportType == 'Warning Not Touched Lead')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId','Lead',Team == 'TAS'?' LastActivityDate < LAST_N_DAYS : '+Days+' '+conditionsMap.get('Warning Lead TAS')+' AND OwnerId IN: OwnerIdList  GROUP BY OwnerId ': ' LastActivityDate < LAST_N_DAYS : '+Days+' '+conditionsMap.get('Warning Lead BPO')+' AND OwnerId IN: OwnerIdList   GROUP BY OwnerId ');
        if(ReportType == 'Leads not touched in 2 days')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId','Lead',Team == 'TAS'?' LastActivityDate < LAST_N_DAYS : 2 '+conditionsMap.get('Warning Lead TAS')+' AND OwnerId IN: OwnerIdList  GROUP BY OwnerId ': ' LastActivityDate < LAST_N_DAYS : 2  '+conditionsMap.get('Warning Lead BPO')+' AND OwnerId IN: OwnerIdList   GROUP BY OwnerId ');
        if(ReportType == 'Leads not touched in 5 days')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId','Lead',Team == 'TAS'?' LastActivityDate < LAST_N_DAYS : 5 '+conditionsMap.get('Warning Lead TAS')+' AND OwnerId IN: OwnerIdList  GROUP BY OwnerId ': ' LastActivityDate < LAST_N_DAYS : 5 '+conditionsMap.get('Warning Lead BPO')+' AND OwnerId IN: OwnerIdList   GROUP BY OwnerId ');
        if(ReportType == 'Leads not touched in 10 days')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId','Lead',Team == 'TAS'?' LastActivityDate < LAST_N_DAYS : 10 '+conditionsMap.get('Warning Lead TAS')+' AND OwnerId IN: OwnerIdList  GROUP BY OwnerId ': ' LastActivityDate < LAST_N_DAYS : 10 '+conditionsMap.get('Warning Lead BPO')+' AND OwnerId IN: OwnerIdList   GROUP BY OwnerId ');
        // Prospecting
        if(ReportType == 'Number Of ImpotrtedLeads')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id , OwnerId','Lead',' DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate  '+conditionsMap.get('Number Of Impotrted Leads')+'  AND OwnerId IN: OwnerIdList GROUP BY OwnerId');
        if(ReportType == 'Calls Made')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Task',' OwnerId IN: OwnerIdList '+conditionsMap.get('Calls Made')+' AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate GROUP BY OwnerId');
        if(ReportType == 'Email Sent')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Task','OwnerId IN: OwnerIdList '+conditionsMap.get('Email Sent')+'  AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate  GROUP BY OwnerId');
        if(ReportType == 'Email Open')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Task','OwnerId IN: OwnerIdList '+conditionsMap.get('Email Opened')+' AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate  GROUP BY OwnerId');
        if(ReportType == 'Converted Lead')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Lead','OwnerId IN: OwnerIdList AND DAY_ONLY(convertTimezone(CreatedDate)) >=: StartDate AND DAY_ONLY(convertTimezone(CreatedDate)) <=: EndDate AND IsConverted = true '+conditionsMap.get('Converted Lead')+' GROUP BY OwnerId'); 
        if(ReportType == 'OPTs Amount')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'SUM(Amount)amt, OwnerId','Opportunity','OwnerId IN: OwnerIdList  '+conditionsMap.get('OPTs Amount')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId');
        if(ReportType == 'Closed Won')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, OwnerId','Opportunity','OwnerId IN: OwnerIdList  '+conditionsMap.get('Closed Won')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId');
        
        // Upsell Report
        if(ReportType == 'Upsell Report')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, SUM(Total_Toll_Free_Numbers__c)TotalTollFreeNumber,SUM(Local_TFN_Amount__c)LocalTFNAmt, COUNT(Extra_Requirements__c)ExtraReq, SUM(Extra_Requirement_Charges__c)ExtraReqCharge, SUM(Client_Portal_Locations__c)ClientPortalLoc, SUM(Client_Portal_Premier_Locations__c)ClientPortalPremierLoc, SUM(Total_Users__c)TotalSecureTextUser ,OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Local Number Of TFN')arList = getAggregateData(StartDate,EndDate,OwnerIdList,'Count(Id)id, SUM(Total_Toll_Free_Numbers__c)TotalTollFreeNumber, COUNT(Extra_Requirements__c)ExtraReq, SUM(Extra_Requirement_Charges__c)ExtraReqCharge,OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND Local_TFN_Amount__c != 0   AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        

        if(ReportType == 'Local/TFN Amount')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' SUM(Local_TFN_Amount__c)amt, OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Client Portal Locations')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' SUM(Client_Portal_Locations__c)amt, OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Client Portal Premier Locations')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' SUM(Client_Portal_Premier_Locations__c)amt, OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Total Secure Text Users')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' SUM(Total_Users__c)amt ,OwnerId','Opportunity',' OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        
        if(ReportType == 'Local or TFN Numbers Needed')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' SUM(Total_Toll_Free_Numbers__c)amt,OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND Local_TFN_Amount__c != 0   AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Extra Requirements')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' COUNT(Extra_Requirements__c)id ,OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND Local_TFN_Amount__c != 0   AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        if(ReportType == 'Extra Requirement Charges')arList = getAggregateData(StartDate,EndDate,OwnerIdList,' SUM(Extra_Requirement_Charges__c)amt,OwnerId','Opportunity','OwnerId IN: OwnerIdList '+conditionsMap.get('Local Number Of TFN')+' AND Local_TFN_Amount__c != 0   AND CloseDate >=: StartDate AND CloseDate <=: EndDate GROUP BY OwnerId ');
        return arList;
    }

    
    
    public static List<AggregateResult> getAggregateData(Date StartDate,Date EndDate, List<String> OwnerIdList,String AggregateFields, String Obj, String WhereCondition){
        
        String aggregateQuery = '';
        aggregateQuery = ' SELECT ' + AggregateFields + ' FROM ' + Obj +'  WHERE  '+ WhereCondition;
        System.debug('aggregateQuery');
        System.debug(aggregateQuery);
        try{
            return Database.query(aggregateQuery); 
        }catch(exception e){
            return null;
        }
    }
}