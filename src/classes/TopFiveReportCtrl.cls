public with sharing class TopFiveReportCtrl {
    public Map<Id,topFiveReortWrapper> userOppMap {set;get;}
    public Map<Id,String> userMap {set;get;}
    public Decimal topFiveTotal {set;get;}
    public Decimal monthlyKPITotal {set;get;}
    public Decimal currentTotal {set;get;}
    public Decimal projectTotal {set;get;}
    public Decimal leftTotal {set;get;}
    public Decimal toGoalTotal {set;get;}
    public Integer currentMonth = Date.today().month();
    public Integer currentyear = Date.today().year();

    public TopFiveReportCtrl(){
        userOppMap = new Map<Id,topFiveReortWrapper>();
        userMap = new Map<Id,String>();
        Map<String,Dashboard_Report_User_Report__c> userCSMap = new Map<String,Dashboard_Report_User_Report__c>();
        userCSMap = Dashboard_Report_User_Report__c.getAll();
        topFiveTotal = 0;
        monthlyKPITotal = 0;
        currentTotal = 0;
        projectTotal = 0;
        leftTotal = 0;

        for(User us : [select Id,Name from User where Name IN: userCSMap.keySet()]){
            userMap.put(us.Id,us.Name);
        }

        for(Opportunity op : [select Id,Name,NextStep,Amount,OwnerId,StageName,Owner.Name,CloseDate from Opportunity where OwnerId IN: userMap.keySet() and (StageName = 'Closed Won' OR StageName = 'Project-Closed Won' OR StageName = 'Top Five')]){
            Integer createdDateMonth = op.CloseDate.month();
            Integer createdDateyear = op.CloseDate.year();
            if(userOppMap.containsKey(op.OwnerId)){
                topFiveReortWrapper tfrw = new topFiveReortWrapper(); 
                //Populate Monthly KPI
                if(userCSMap.containsKey(op.Owner.Name)){
                    Decimal monthlyKPI = 0;
                    monthlyKPI = userCSMap.get(op.Owner.Name).Monthly_KPI__c;
                    tfrw.monthlyKPI = monthlyKPI;
                }
                tfrw = userOppMap.get(op.OwnerId);
                if(op.StageName == 'Top Five'){
                    tfrw.topFiveOpts.add(op);
                }else if(op.StageName == 'Closed Won' && op.Amount != null && createdDateMonth == currentMonth && createdDateyear == currentyear){
                    tfrw.Current += op.Amount;
                }else if(op.StageName == 'Project-Closed Won' && op.Amount != null && createdDateMonth == currentMonth && createdDateyear == currentyear){
                    tfrw.Project += op.Amount;
                }
            }else{
                topFiveReortWrapper tfrw = new topFiveReortWrapper();    
                //Populate Monthly KPI
                if(userCSMap.containsKey(op.Owner.Name)){
                    Decimal monthlyKPI = 0;
                    monthlyKPI = userCSMap.get(op.Owner.Name).Monthly_KPI__c;
                    tfrw.monthlyKPI = monthlyKPI;
                }
                if(op.StageName == 'Top Five'){
                    tfrw.topFiveOpts.add(op);
                }else if(op.StageName == 'Closed Won' && op.Amount != null && createdDateMonth == currentMonth && createdDateyear == currentyear){
                    tfrw.Current += op.Amount;
                }else if(op.StageName == 'Project-Closed Won' && op.Amount != null && createdDateMonth == currentMonth && createdDateyear == currentyear){
                    tfrw.Project += op.Amount;
                }
                userOppMap.put(op.OwnerId,tfrw);
            }
        }

        for(Id ids : userOppMap.keySet()){
                topFiveReortWrapper tfR = userOppMap.get(ids);
                tfR.leftToGo = tfR.monthlyKPI - tfR.Current;
                if(tfR.Current == 0){
                    tfR.toGoal = 100;
                }else{
                    tfR.toGoal = ((tfR.Current*100)/tfR.monthlyKPI);
                }
                
                Decimal topTo = 0;
                for(Opportunity op : tfR.topFiveOpts){
                    if(op.Amount != null){
                        topTo += op.Amount;
                    }
                }
                tfR.topFiveOppTotal = topTo;
                topFiveTotal += topTo;
                monthlyKPITotal += tfR.monthlyKPI;
                currentTotal += tfR.Current;
                projectTotal += tfR.Project;
                leftTotal += tfR.leftToGo;
        }
        toGoalTotal = ((currentTotal*100)/monthlyKPITotal);
    }
    public PageReference exportData(){
        Pagereference redirectedPage = new Pagereference('/apex/TopFiveExcel');
        // Pagereference redirect = new Pagereference('apex/LeadConversionReport');
        redirectedPage.setRedirect(true); 
        return redirectedPage;
    }

    public class topFiveReortWrapper{
        public set<Opportunity> topFiveOpts {set;get;}
        public Decimal topFiveOppTotal {set;get;}
        public Decimal monthlyKPI {set;get;}
        public Decimal Current {set;get;}
        public Decimal Project {set;get;}
        public Decimal leftToGo {set;get;}
        public Decimal toGoal {set;get;}

        public topFiveReortWrapper(){
            topFiveOpts = new set<Opportunity>();
            topFiveOppTotal = 0;
            monthlyKPI = 0;
            Current = 0;
            Project = 0;
            leftToGo = 0;
            toGoal = 0;
        }
    }
}