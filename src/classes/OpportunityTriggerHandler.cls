public class OpportunityTriggerHandler {
    
    List<Opportunity> recordNewList = new List<Opportunity>();
    List<Opportunity> recordOldList = new List<Opportunity>();
    Map<Id, Opportunity> recordNewMap = new Map<Id, Opportunity>();
    Map<Id, Opportunity> recordOldMap = new Map<Id, Opportunity>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    public static boolean recursionController = false;
    
	public OpportunityTriggerHandler(List<Opportunity> newList, List<Opportunity> oldList, Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){
        System.debug('recordNewList before'+recordNewList);
        OnconvertTypeChange(recordNewList);
    }
    
    public void BeforeUpdateEvent(){}
    
    public void BeforeDeleteEvent(){}
    
    public void AfterInsertEvent(){
 
    }
    
    public void AfterUpdateEvent(){
        
    }
    
    public void AfterDeleteEvent(){}
    
    public void AfterUndeleteEvent(){}
    
    public void OnconvertTypeChange(List<Opportunity> recordNewList){
        
        System.debug(recordNewList);
        
        for(Opportunity op : recordNewList){
            if(op.IsConverted__c == true){
                op.Type = 'New Business';
            }
        }
        
        System.debug('recordNewList after update'+recordNewList);
    }
    
}