@isTest
public class TestCaseTrigger {  
    	public static testMethod void test() {
    	    
    	    Account acc = new Account(name='test');
            insert acc;
    	    
    	    Contact con = new Contact(LastName='test',AccountId=acc.id,Email='test@test.com');
    	    insert con;
    	    
    	    Case c = new Case(ContactId=con.id,Send_Email_with_Status_Change__c = true,Status='Open',Type='Billing Inquiry',Origin='Email',Description='test');
    	    insert c;
    	}
}