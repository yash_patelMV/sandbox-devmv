<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Plantilla de HelloSign</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Plantillas de HelloSign</value>
    </caseValues>
    <fields>
        <label><!-- Allow Additional Attachments --></label>
        <name>HelloSign__AllowAdditionalFiles__c</name>
    </fields>
    <fields>
        <help><!-- The ability to have a signer reassign the signature to another person. --></help>
        <label><!-- Allow Reassignment --></label>
        <name>HelloSign__AllowReassign__c</name>
    </fields>
    <fields>
        <help><!-- This field is checked automatically by the archiving button when the template is removed from HelloSign.  This prevents one-sided archiving. --></help>
        <label><!-- Allow Archiving --></label>
        <name>HelloSign__Allow_Archiving__c</name>
    </fields>
    <fields>
        <help><!-- This field records the method of how the Signature Request will be delivered --></help>
        <label><!-- Delivery Method --></label>
        <name>HelloSign__Delivery_Method__c</name>
        <picklistValues>
            <masterLabel>Request in Person</masterLabel>
            <translation>Solicitar en persona</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Request via Email</masterLabel>
            <translation>Solicitar por correo electrónico</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Running User Signs</masterLabel>
            <translation>Firmas de usuario activo</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Dynamic File Name Contains --></label>
        <name>HelloSign__Dynamic_File_Name_Contains__c</name>
    </fields>
    <fields>
        <label><!-- Dynamic File --></label>
        <name>HelloSign__Dynamic_File__c</name>
    </fields>
    <fields>
        <label><!-- External Template Type --></label>
        <name>HelloSign__External_Template_Type__c</name>
    </fields>
    <fields>
        <label><!-- For Embedded Signing --></label>
        <name>HelloSign__For_Embedded_Signing__c</name>
    </fields>
    <fields>
        <label><!-- Had Significant Changes --></label>
        <name>HelloSign__HadSignificantChanges__c</name>
    </fields>
    <fields>
        <help><!-- If checked, the signers are sequential based on the signer order.  If unchecked, all signers are in parallel regardless of the signer order. --></help>
        <label><!-- Signers are Sequential --></label>
        <name>HelloSign__IsSignersSequential__c</name>
    </fields>
    <fields>
        <label><!-- Lock Dynamic File --></label>
        <name>HelloSign__Lock_Dynamic_File__c</name>
    </fields>
    <fields>
        <help><!-- The merge fields that are available in the template. --></help>
        <label><!-- Merge Fields --></label>
        <name>HelloSign__MergeFields__c</name>
    </fields>
    <fields>
        <label><!-- Message --></label>
        <name>HelloSign__Message2__c</name>
    </fields>
    <fields>
        <help><!-- Field deprecated. Please update to use Message2__c. --></help>
        <label><!-- Message --></label>
        <name>HelloSign__Message__c</name>
    </fields>
    <fields>
        <label><!-- Optional Cc --></label>
        <name>HelloSign__Optional_CC__c</name>
    </fields>
    <fields>
        <help><!-- Restricting Title and Message will not allow a requestor to change the Title and Message when sending for signature --></help>
        <label><!-- Restrict Title And Message --></label>
        <name>HelloSign__RestrictTitleAndMessage__c</name>
        <picklistValues>
            <masterLabel>Restrict Both</masterLabel>
            <translation>Restringir ambos</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Restrict Message</masterLabel>
            <translation>Restringir mensaje</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Restrict None</masterLabel>
            <translation>No restringir</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Restrict Title</masterLabel>
            <translation>Restringir título</translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Skip Prepare --></label>
        <name>HelloSign__SkipPrepare__c</name>
    </fields>
    <fields>
        <help><!-- Request sent with Skip Preview enabled will not show a preview screen for the request, but rather send without a preview --></help>
        <label><!-- Skip Preview --></label>
        <name>HelloSign__SkipPreview__c</name>
    </fields>
    <fields>
        <help><!-- The object that the template is used for (API Name) --></help>
        <label><!-- Source Object --></label>
        <name>HelloSign__SourceObject__c</name>
    </fields>
    <fields>
        <help><!-- The status of the template.  Includes: Draft, Pending, Active, Error, Archived.  Note this is set by automation and should not be changed manually. --></help>
        <label><!-- Status --></label>
        <name>HelloSign__Status__c</name>
    </fields>
    <fields>
        <help><!-- Unique identifier used to link templates with HelloSign --></help>
        <label><!-- Template UUID --></label>
        <name>HelloSign__TemplateUUID__c</name>
    </fields>
    <fields>
        <label><!-- Template Type --></label>
        <name>HelloSign__Template_Type__c</name>
        <picklistValues>
            <masterLabel>External</masterLabel>
            <translation>Externo</translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Salesforce</masterLabel>
            <translation><!-- Salesforce --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- If a dynamic file is used for this template, does that file include text tags? --></help>
        <label><!-- Text Tags Enabled --></label>
        <name>HelloSign__Text_Tags_Enabled__c</name>
    </fields>
    <fields>
        <help><!-- This title will auto populate at time to send for signature --></help>
        <label><!-- Title --></label>
        <name>HelloSign__Title__c</name>
    </fields>
    <fields>
        <help><!-- The number of related signer roles. --></help>
        <label><!-- Total Signer Roles --></label>
        <name>HelloSign__TotalSignerRoles__c</name>
    </fields>
    <fields>
        <help><!-- The merge fields that are writeback enabled in the template. --></help>
        <label><!-- Writeback Fields --></label>
        <name>HelloSign__Writeback_Fields__c</name>
    </fields>
    <gender>Masculine</gender>
    <nameFieldLabel>Nombre de la plantilla</nameFieldLabel>
    <recordTypes>
        <description><!-- This record type is used for documents generated outside of HelloSign to be sent via HelloSign --></description>
        <label><!-- External --></label>
        <name>HelloSign__External</name>
    </recordTypes>
    <recordTypes>
        <description><!-- This record type is used for HelloSign-generated documents only --></description>
        <label><!-- HelloSign --></label>
        <name>HelloSign__HelloSign</name>
    </recordTypes>
    <validationRules>
        <errorMessage><!-- Templates must be archived using the &quot;Archive Template&quot; button. --></errorMessage>
        <name>HelloSign__Restrict_Archiving_To_Button</name>
    </validationRules>
    <webLinks>
        <label><!-- ArchiveTemplate --></label>
        <name>HelloSign__ArchiveTemplate</name>
    </webLinks>
    <webLinks>
        <label><!-- Edit_Template --></label>
        <name>HelloSign__Edit_Template</name>
    </webLinks>
</CustomObjectTranslation>
