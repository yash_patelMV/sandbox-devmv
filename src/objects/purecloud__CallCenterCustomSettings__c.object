<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>List</customSettingsType>
    <description>Custom settings for the Call Center</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>purecloud__Auto_Assign_Owner__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Auto Assign Email-to-Case Owner</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__CTI_Extension_Apex_Class__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>CTI Extension Apex Class</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__Client_Event_Message_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Client Event Message Type enables client events through the postMessage API or Lightning Message Service.</inlineHelpText>
        <label>Client Event Message Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__Default_Outbound_SMS_Country_Code__c</fullName>
        <deprecated>false</deprecated>
        <description>Specify a default country code to show in the PureCloud for Salesforce client UI when an agent creates an outbound SMS message.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify a default country code to show in the PureCloud for Salesforce client UI when an agent creates an outbound SMS message.</inlineHelpText>
        <label>Default Outbound SMS Country Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__Embed_Scripts_in_Console__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to display scripts in a console component. Assign the PureCloudScript console component to the console apps that you want to use the component.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to display scripts in a console component. Assign the PureCloudScript console component to the console apps that you want to use the component.</inlineHelpText>
        <label>Embed Interaction Window</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Auto_Association__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to automatically associate a Salesforce record with the interaction’s call log.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to automatically associate a Salesforce record with the interaction’s call log.</inlineHelpText>
        <label>Enable Auto Association on Navigation</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Call_History__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to enable Call History view in PureCloud for Salesforce client.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to enable Call History view in PureCloud for Salesforce client.</inlineHelpText>
        <label>Enable Call History</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Call_Log_Functionality__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to access interaction logs from the client and pop interaction logs when interactions are connected.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to access interaction logs from the client and pop interaction logs when interactions are connected.</inlineHelpText>
        <label>Enable Interaction Logs</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Call_Logs__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to write and edit interaction logs in the client. Because of problems Internet Explorer has saving interaction logs, if using Internet Explorer, clear this setting.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to write and edit interaction logs in the client. Because of problems Internet Explorer has saving interaction logs, if using Internet Explorer, clear this setting.</inlineHelpText>
        <label>Enable Interaction Log Editing</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Chat_Messages__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Expand Chat Notification is available after you add Notification to Client Event Types.</inlineHelpText>
        <label>Expand Chat Notification</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Client_Events__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Enable Client Events broadcasts client events through the postMessage API.</description>
        <externalId>false</externalId>
        <inlineHelpText>Enable Client Events broadcasts client events through the postMessage API.</inlineHelpText>
        <label>Enable Client Events</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Configurable_CallerID__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check this setting to allow agents to enter a CallerID Name and Number for an outbound call.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this setting to allow agents to enter a CallerID Name and Number for an outbound call.</inlineHelpText>
        <label>Enable Configurable Caller ID</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Dedicated_Login_Window__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to allow agents to login using a separate window.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to allow agents to login using a separate window.</inlineHelpText>
        <label>Enable Dedicated Login Window</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Disconnect_on_Tab_Close__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Disconnect an interaction when an associated Salesforce object (tab) is closed in Salesforce Console mode.</description>
        <externalId>false</externalId>
        <inlineHelpText>Disconnect an interaction when an associated Salesforce object (tab) is closed in Salesforce Console mode.</inlineHelpText>
        <label>Enable Disconnect on Tab Close</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_High_Velocity_Sales__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Enable High Velocity Sales</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Omni_Sync__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to sync statuses between Omni-Channel and PureCloud for Salesforce.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to sync statuses between Omni-Channel and PureCloud for Salesforce.</inlineHelpText>
        <label>Enable Omni-Channel Sync</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_SSO__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to use an SSO identity provider for login.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to use an SSO identity provider for login.</inlineHelpText>
        <label>Auto Redirect to SSO</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Server_Side_Logging__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Enable server-side logging</description>
        <externalId>false</externalId>
        <inlineHelpText>Enable server-side logging</inlineHelpText>
        <label>Enable Server-Side Logging</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Tab_Interaction_Sync__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Enable Tab/Interaction Sync</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Enable_Workspace_Transfers__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to transfer in Service Cloud the main tab that an agent is viewing and any subtabs for that record that an agent has open.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to transfer in Service Cloud the main tab that an agent is viewing and any subtabs for that record that an agent has open.</inlineHelpText>
        <label>Enable Workspace Transfers</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Is_Sandbox__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Is Sandbox</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Organization_Id__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Organization Id</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__Organization_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>Add the name of the Genesys Cloud organization to use the SSO identity provider for login.</description>
        <externalId>false</externalId>
        <inlineHelpText>Add the name of the Genesys Cloud organization to use the SSO identity provider for login.</inlineHelpText>
        <label>Genesys Cloud Organization Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__Organization_Type__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Organization Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__SSO_Provider_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>Add the name of the SSO identity provider to use for login.</description>
        <externalId>false</externalId>
        <inlineHelpText>Add the name of the SSO identity provider to use for login.</inlineHelpText>
        <label>SSO Identity Provider Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__Salesforce_Organization_Name__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Salesforce Organization Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>purecloud__Search_External_Contacts__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to search for Genesys Cloud external contacts in the client when transferring interactions or making calls.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to search for Genesys Cloud external contacts in the client when transferring interactions or making calls.</inlineHelpText>
        <label>Search Genesys Cloud External Contacts</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Search_People__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to search for Genesys Cloud people in the client when transferring interactions or making calls.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to search for Genesys Cloud people in the client when transferring interactions or making calls.</inlineHelpText>
        <label>Search Genesys Cloud People</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Search_Queues__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to search for Genesys Cloud queues in the client when transferring interactions or making calls.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to search for Genesys Cloud queues in the client when transferring interactions or making calls.</inlineHelpText>
        <label>Search Genesys Cloud Queues</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Search_Salesforce__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Select this setting to search for Salesforce records in the client when transferring interactions or making calls.</description>
        <externalId>false</externalId>
        <inlineHelpText>Select this setting to search for Salesforce records in the client when transferring interactions or making calls.</inlineHelpText>
        <label>Search Salesforce Records</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>purecloud__Strip_Country_Codes__c</fullName>
        <deprecated>false</deprecated>
        <description>List of country codes to strip off of a phone number in order to perform screen pop more effectively.</description>
        <externalId>false</externalId>
        <label>Strip Country Codes</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Custom Call Center Settings</label>
    <visibility>Public</visibility>
</CustomObject>
