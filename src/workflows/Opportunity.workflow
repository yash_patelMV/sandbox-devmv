<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Cancellation_Email_To_Team_Andicott</fullName>
        <ccEmails>cancellations@endicottcomm.com</ccEmails>
        <description>Cancellation Email To Team Andicott</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/Cancellation_alert_to_the_team</template>
    </alerts>
    <alerts>
        <fullName>Cancellation_Email_To_Team_Ansafone</fullName>
        <ccEmails>cancellations@endicottcomm.com</ccEmails>
        <description>Cancellation Email To Team Ansafone</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/Cancellation_alert_to_the_team</template>
    </alerts>
    <alerts>
        <fullName>X30_Days_Follow_up</fullName>
        <description>30 Days Follow up</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/X30_Day_Email_to_Client</template>
    </alerts>
    <alerts>
        <fullName>X60_Days_Follow_up</fullName>
        <description>60 Days Follow up</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/X60_Day_Email_to_Client</template>
    </alerts>
    <alerts>
        <fullName>X90_Days_Follow_up</fullName>
        <description>90 Days Follow up</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/X90_Day_Email_to_Client</template>
    </alerts>
    <fieldUpdates>
        <fullName>Opportunity_Type_Field_Update</fullName>
        <field>Type</field>
        <literalValue>New Business</literalValue>
        <name>Opportunity Type Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Type_Field_Updates</fullName>
        <field>Type</field>
        <literalValue>New Business</literalValue>
        <name>Opportunity Type Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
