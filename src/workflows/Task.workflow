<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_To_Christina_Chamblee_For_Phone_Number_Assignment</fullName>
        <ccEmails>cchamblee@ansafone.com</ccEmails>
        <description>Email To Christina Chamblee For Phone Number Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/New_Phone_Account_Number_to_Christina_Chamblee</template>
    </alerts>
    <alerts>
        <fullName>Welcome_Email</fullName>
        <description>Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>X30_Days_After_Start_Date_Email_To_Client</fullName>
        <description>30 Days After Start Date Email To Client</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/X30_Day_Email_to_Client</template>
    </alerts>
    <alerts>
        <fullName>X60_Days_After_Start_Date_Email_To_Client</fullName>
        <description>60 Days After Start Date Email To Client</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/X60_Day_Email_to_Client</template>
    </alerts>
    <alerts>
        <fullName>X90_Days_After_Start_Date_Email_To_Client</fullName>
        <description>90 Days After Start Date Email To Client</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/X90_Day_Email_to_Client</template>
    </alerts>
</Workflow>
