<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Notification_to_Admin_for_New_Lead</fullName>
        <description>Send Notification to Admin for New Lead</description>
        <protected>false</protected>
        <recipients>
            <recipient>ehealy@ansafone.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Send_Notification_to_Lead_Owner_for_New_Lead</fullName>
        <description>Send Notification to Lead Owner for New Lead</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_Email_Template/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Phone_Field_Update</fullName>
        <field>Phones__c</field>
        <formula>Phone</formula>
        <name>Phone Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_field</fullName>
        <field>Email__c</field>
        <formula>Email</formula>
        <name>Update Email field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
