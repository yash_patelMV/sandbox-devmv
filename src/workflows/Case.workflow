<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Email_on_Case_Status_to_Escalation</fullName>
        <description>Case : Email on Case Status to Escalation</description>
        <protected>false</protected>
        <recipients>
            <recipient>Manager_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_case_module/Case_Status_Change_to_Escalation_Notification_to_Manager</template>
    </alerts>
    <alerts>
        <fullName>Case_New_Case_Notification_to_Account_Executive</fullName>
        <description>Case: New Case Notification to Account Executive</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_case_module/Case_New_Case_Notify_to_Account_executives</template>
    </alerts>
    <alerts>
        <fullName>Case_New_Case_Notification_to_Manager</fullName>
        <description>Case: New Case Notification to Manager</description>
        <protected>false</protected>
        <recipients>
            <recipient>Manager_Group</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>Tas_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_case_module/Case_New_Case_Notify_to_Manager_Queue</template>
    </alerts>
    <alerts>
        <fullName>Case_Send_Email_After_2_Weeks_Case_Closed_v1_0</fullName>
        <description>Case: Send Email After 2 Weeks Case Closed v1.0</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_case_module/Case_Send_Email_After_2_Weeks_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>On_Case_Owner_Change_Notify_to_User</fullName>
        <description>On Case Owner Change Notify to User</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Ansafone_case_module/Case_New_Case_Notify_to_Account_executives</template>
    </alerts>
    <fieldUpdates>
        <fullName>Last_Status_Change_Time</fullName>
        <field>Last_Status_Change_Time__c</field>
        <formula>NOW()</formula>
        <name>Last Status Change Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Last_Status_Change_Time_Update</fullName>
        <field>Last_Status_Change_Time__c</field>
        <formula>NOW()</formula>
        <name>Last Status Change Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case %3A Update case assignment email to manager team</fullName>
        <actions>
            <name>Case_New_Case_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Last_Status_Change_Time_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OwnerId  =  $Label.Manager_Queue_Id,  ISCHANGED(OwnerId)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case %3A new Case assignment email to Account executive</fullName>
        <actions>
            <name>Case_New_Case_Notification_to_Account_Executive</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Last_Status_Change_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISPICKVAL( Status , &apos;Open&apos;) ,  NOT(ISBLANK(Opportunity__r.Account_Executive__c)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case %3A new Case assignment email to manager team</fullName>
        <actions>
            <name>Case_New_Case_Notification_to_Manager</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OwnerId == $Label.Manager_Queue_Id</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Owner Change notify to User</fullName>
        <actions>
            <name>On_Case_Owner_Change_Notify_to_User</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>AND( ISNEW() == False,  ISCHANGED( OwnerId  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create Task after case close if email is not listed</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>follow_up_with_contact_from_recent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Case.ClosedDate</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send Email After Close Case v1%2E0</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Resolved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Disable_2_Week_Follow_Up_Email__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Email_with_Status_Change__c</field>
            <operation>notEqual</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Send_Email_After_2_Weeks_Case_Closed_v1_0</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.ClosedDate</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>follow_up_with_contact_from_recent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>follow up with contact from recent</subject>
    </tasks>
</Workflow>
